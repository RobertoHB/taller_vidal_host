<?php

namespace app\controllers;

use Yii;
use app\models\Parte1;
use app\models\Parte1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Parte1Controller implements the CRUD actions for Parte1 model.
 */
class Parte1Controller extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Parte1 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Parte1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Parte1 model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Parte1 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($vehiculo=NULL)
    {
        $model = new Parte1();
        
        $data_autocomplete = $model->getVehiculos_autocomplete();
        $repuestos_autocomplete = $model->getRepuestos_autocomplete();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }else{
             $model->fecha_presupuesto = date('d-m-Y');
        }

        if (isset($vehivulo)){
            $model->vehiculo = $vehiculo;
          }
        return $this->render('create', [
            'model' => $model,
            'datos_autocomplete' => $data_autocomplete,
            'repuestos_autocomplete' => $repuestos_autocomplete,
        ]);
    }
  

    /**
     * Updates an existing Parte1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $repuestos_autocomplete = $model->getRepuestos_autocomplete();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'repuestos_autocomplete' => $repuestos_autocomplete,
        ]);
    }

    /**
     * Deletes an existing Parte1 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Parte1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Parte1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parte1::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
