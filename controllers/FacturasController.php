<?php

namespace app\controllers;

use Yii;
use app\models\Facturas;
use app\models\FacturasSearch;
use app\models\parte1;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\data\SqlDataProvider;
/**
 * FacturasController implements the CRUD actions for Facturas model.
 */
class FacturasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Facturas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacturasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Facturas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Facturas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Facturas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Facturas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $busco_parte = parte1::findOne($model->parte);

            $busco_parte->nfactura = $model->factura;
            $busco_parte->salida = $model->fecha;
            $busco_parte->save();
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Facturas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($factura)
    {
       $factura_eliminar = $this->findModel($factura);

       
       $model = new Facturas();
       
        $ultimaFactura= Facturas::find()
                        ->max('factura');   
                    
        if(empty($ultimaFactura) or $ultimaFactura == 0){
            $model->factura = 1;
        }else{    
            $model->factura = $ultimaFactura + 1;
        }
       $model->parte = $factura_eliminar->parte;
       $model->fecha = date("Y-m-d");
       $model->tipoiva = $factura_eliminar->tipoiva;
       $model->iva = $factura_eliminar->iva * -1;
       $model->subtotal = $factura_eliminar->subtotal * -1;
       $model->total = $factura_eliminar->total * -1;
       $model->comentarios = "Esta factura anula la ".$factura_eliminar->factura;
       $model->save();
       return $this->redirect(['index']);
    }

    /**
     * Finds the Facturas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Facturas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Facturas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
      public function actionFacturar()
    {
          $model = new Facturas();
 
          
         if (Yii::$app->request->post('idparte')){
             
             //Calculamos si las facturas va correlativas o hay algun salto
             //Si lo hay proseguimos con la numeración correlativa
            //$ultimaFactura= Facturas::find()
              //              ->max('factura');   
             $todas_facturas = Facturas::find()
                                ->select('factura')
                                ->asArray()
                                ->orderBy('factura asc')
                                ->all();
             
             
            foreach ($todas_facturas as $key => $value) { 
               // echo("Indice: ".$key." Valor: ".$value['factura']);
                
                if($key == 0 ){
                    $anterior = $value['factura'];
                }else{
                    if($value['factura'] == $anterior + 1){
                        $anterior = $value['factura'];
                    }else{    
                        $ultimaFactura = $anterior;
                    }
                }
            }
             
            if(empty($ultimaFactura) or $ultimaFactura == 0){
                $model->factura = 1;
            }else{    
                $model->factura = $ultimaFactura + 1;
                 //acualizamos el registro de parte asignandole la nueva factura generada
                $model_parte1 = new parte1();
                $actualizar_nfactura = $model_parte1->findOne(Yii::$app->request->post('idparte'));
                $actualizar_nfactura->nfactura =  $model->factura;
                $actualizar_nfactura->save();
            }
          
           
          
            $model->parte =  Yii::$app->request->post('idparte');
            //$base = Yii::$app->request->post('basefactura');          
            $model->tipoiva =  Yii::$app->request->post('tipoiva');
            $model->subtotal = Yii::$app->request->post('subtotal');
            $model->iva = Yii::$app->request->post('ivafactura');
            $model->total = Yii::$app->request->post('totalfactura');
            $model->fecha = Yii::$app->request->post('fechasalida');
            $model->save();
            return $this->redirect(['index']);
         }
         //$datos= array(['parte'=>$parte,'base'=>$base,'tipo_iva'=>$tipoIva,'subtotal'=>$subtotal,'iva'=>$iva,'total'=>$total]);
        //return json_encode($datos);
      
    }
    
    public function actionFacturacion(){
        //SELECT SUM(total) as total, DATE_FORMAT(fecha,'%M','en-es')as mes FROM facturas WHERE YEAR(fecha) = 2021 GROUP BY month(fecha)
        $anyo = $_POST['anyo'];
    
        $data = new SqlDataProvider([
                'sql' => 'SELECT SUM(total)as total, month(fecha) as mes FROM facturas WHERE YEAR(fecha) ='. $anyo.' GROUP BY month(fecha)',
        
        ]);
//        
//        $data = Facturas::find()
//                ->select('fecha', 'total')
//                ->from('facturas')
//                ->where(['Year(fecha)' => $anyo])
//                ->groupBy(['fecha'])
               // ->asArray()
//                ->all();
        $array_datos = $data->getModels();
//        var_dump($array_datos[0]['mes']);
//        exit;
//        
//        return($data);
//        exit;
     
        foreach ($array_datos as  $key => $value) {
           $array_datos[$key]['mes'] = $this->TraducirMes($value['mes']); 
        }

        $datos = JSON::encode($array_datos);
        return $datos;
    }
    
      public function actionEnviarfacturas()
    {
          
           $html = '
                <html lang="en">
                <head>
                        <meta charset="UTF-8">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                        <title>Envio correo Facturación</title>
                        <style type="text/css">


                        </style>
                </head>
                <body style="max-width:1200px;">    

                <header width="100%" align="center">
                       
                        <label style="padding:40px 100px 40px 100px; font-weight: bold;font-size:30px;">Taller JVidal</label>
                       
                       
                </header>

                <main width="100%" align="center">
                        <div style="text-align:center;color:#c62828;font-weight: bold;font-size:30px;">
                            <p style="">Resumen de Facturación </p>
                        </div>
                </main>

                <footer width="100%" align="center" style="margin-top:35px;">
                </footer>
                </body>
                </html>';
            
         $contenido = Yii::$app->mailer->compose();
           $destino = "robherblanc@gmail.com";
              // $contenido->attach($path.'/'.'lg_ayuntamiento.png');
                //->attach($path.'/'.'nif.jpg')
                //->attach($path.'/'.'firma.png')    
                //->attachContent('Contenido adjunto', ['fileName' => 'attach.txt', 'contentType' => 'text/plain'])
                $contenido->setTo($destino);
                $contenido->setFrom([Yii::$app->params['senderEmail']]);
                $contenido->setReplyTo([Yii::$app->params['senderEmail']]);
                $contenido->setSubject('Facturacion taller vidal');
                $contenido->setTextBody('');
                $contenido->setHtmlBody($html);
                $contenido->send();
          return "Correo enviado";
          
    }
      
   public function TraducirMes($mes){
    switch ($mes) {
        case 1:
            return "Enero";
            break;

         case 2:
            return "Febrero";
            break;
         case 3:
            return "Marzo";
            break;
         case 4:
            return "Abril";
            break;
         case 5:
            return "Mayo";
            break;
         case 6:
            return "Junio";
            break;
         case 7:
            return "Julio";
            break;
         case 8:
            return "Agosto";
            break;
         case 9:
            return "Septiembre";
            break;
         case 10:
            return "Octubre";
            break;
         case 11:
            return "Noviembre";
            break;
         case 12:
            return "Diciembre";
            break;
    }
       
       
   }
   
}
