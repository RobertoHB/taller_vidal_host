<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Facturas;
use app\models\FacturasSearch;
use app\models\FiltroExporta;
use yii\swiftmailer;
use Mpdf\Mpdf;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use \DateTime;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
     public function actionInformes(){
         
         
         $model = new FiltroExporta();
         $query = Facturas::find()
                  ->select(['parte','factura','fecha','tipoiva','iva','subtotal','total'])
                 // ->where(['between', 'fecha', $fecha_inicio, $fecha_fin])
                  ->where(['fecha' => '']);
                  
             $searchModel = new FacturasSearch();
         
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
               // 'pagination' => false,
            ]);     
     //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         
     $gridColumns = [//['class' => 'yii\grid\SerialColumn'],
                           //'id',
                           'parte',
                           'factura',
                           'fecha',
                           'tipoiva',
                            'iva',
                            'subtotal',
                            'total',
                           ['class' => 'yii\grid\ActionColumn'],
                       ];

         
        return $this->render('/site/informes', [
            'model' => $model,
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          'gridColumns' => $gridColumns,
        ]);
         
        
     }
    
    
    
    public function actionPdf_parte($parte,$tipo){
       
          if(isset($parte)){
            $datos = new SqlDataProvider([
                'sql' => "SELECT p.id parte,p.fecha_presupuesto fecha_presu,p.vehiculo vehiculo,p.entrada entrada, p.salida salida,
                                p.nfactura factura, p.descripcion descripcion, p.estado estado ,p.kms kms,p.dto dto,p.iva iva,
                                 v.cliente cliente, v.matricula matricula, v.marca marca, v.color color, v.bastidor bastidor, v.combustible combustible,
                                 c.nombre nombre,c.apellidos apellidos,c.direccion direccion,c.cp cp,c.localidad localidad,c.cif_nif cifnif
                                 FROM parte1 p JOIN vehiculos v ON p.vehiculo = v.id 
                                                    join clientes c ON v.cliente = c.id 
                                                   WHERE p.id = $parte",   
           ]); 
         
  
            $resultado = $datos->getModels();
            
           //Datos cabecera cliente
            $parte = $resultado[0]['parte'];
            $fecha_presu =  $resultado[0]['fecha_presu'];
            $factura =  $resultado[0]['factura'];
            $nombre =  $resultado[0]['nombre'];
            $apellidos =  $resultado[0]['apellidos'];
            $direccion =  $resultado[0]['direccion'];
            $localidad =  $resultado[0]['cp']." ".$resultado[0]['localidad'];
            $cif_nif =  $resultado[0]['cifnif'];
            
            
            //Datos de cabecera del Vehiculo
             $marca =  $resultado[0]['marca'];
             $matricula =  $resultado[0]['matricula'];
             $kms =  $resultado[0]['kms'];
             $bastidor =  $resultado[0]['bastidor'];
            
            //Datos parte reparacion
              $entrada =  $resultado[0]['entrada'];
              $salida =  $resultado[0]['salida'];
              $iva = $resultado[0]['iva'];
              $dto = $resultado[0]['dto'];
              $descripcion =  $resultado[0]['descripcion'];
            
            
            //Datos del detalle de la reparacion
             $detalle_parte = new SqlDataProvider([
                'sql' => "SELECT pd.id_parte1 parte,pd.codigo codigo,pd.descripcion desc_repuesto,pd.cantidad cantidad,
                                pd.dto dto, pd.importe importe
                    FROM parte2 pd WHERE pd.id_parte1 = '$parte'",   
           ]); 
            $resultado_detalle = $detalle_parte->getModels();
            
            
          
            //logo taller
            $logo = Url::to('@web/img/logo.png');
            //$logo_gob =  url::to('@web/img/gobcantabria.jpg');
 
            
        }
            
        
   
   
$contenido = '
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	
</head>
<body>';
      if(($tipo == 'P') || ($tipo == 'F')){
       	$contenido .= '     
          <table id="sinbordes">
              <tr>
                  <td colspan="4" id="sinbordes"><img src="'.$logo.'" alt="Taller J. Vidal" width="240px"></td>
                  <td colspan="3" id="sinbordes" align="right">
                      <p id="cabecera"><b>JOSE ISIDORO VIDAL<br>
                      Barrio La Trapa, 9<br>
                      39718 - Medio Cudeyo<br>
                      CIF: 13768925K<br>
                      TELF: 942522410</b></p>
                  </td>
              </tr>
          </table>';
      }else{       
        $contenido .= '<table id="sinbordes">
		<tr>
        	<td colspan="4" id="sinbordes"><h2>Parte de Reparación</h2></td>
			<td colspan="3" id="sinbordes" align="right"><p id="cabecera"><b>JOSE ISIDORO VIDAL- CIF: 13768925K</p></td>
        </tr>
        <tr>
        	<td colspan="4" id="sinbordes"><p>Plazo máximo de entrega del vehículo 20 días hábiles.</p></td>
        </tr>
       </table>';
      }
      
	$contenido .= '<table>
		<tr>
			<td colspan="5">';
      if($tipo === 'P') $contenido .= '<h3>PRESUPUESTO  '.$parte.'</h3>';
      if($tipo === 'F') $contenido .= '<h3>FACTURA  '.$factura.'</h3>';
      
      $contenido .='
				<p><b>Nombre: </b>'.$nombre." ".$apellidos.'</p>
                <p><b>Dirección: </b>'.$direccion.'</p>
                <p><b>Población: </b>'.$localidad.'</p>
                <p><b>CIF/DNI: </b>'.$cif_nif.'</p>
			</td>
			<td colspan="2">
				<p><b>Fecha:  </b>'.date("d-m-Y", strtotime($fecha_presu)).'</p>
				<p><b>Marca/Modelo: </b>'.$marca.'</p>
				<p><b>Matrícula: </b>'.$matricula.'</p>
                                <p><b>Kms: </b>'.$kms.'</p>    
				<p><b>Chasis: </b>'.$bastidor.'</p>
				
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td colspan="2"><b>CONCEPTOS</b></td>
			<td colspan="2"><b>Fecha Entrada: </b>'.date("d-m-Y", strtotime($entrada)).'</td>
			<td colspan="3"><b>Fecha Salida: </b>'.date("d-m-Y", strtotime($salida)).'</td>
		</tr>
		<tr>
			<td colspan="7">'.$descripcion.'</td>
		</tr>';
  if($tipo == 'R'){ 
    $contenido .=  
      '</table></body></html>';
  }
      
if(($tipo == 'P') || ($tipo == 'F')){
		$contenido .= '<tr>
			<th width="25px">Código</th>
			<th colspan="2">Descripción</th>
			<th width="10px">DTO.</th>
			<th width="25px">CANT.</th>
			<th width="75px">PVP</th>
			<th width="75px">TOTAL</th>
		</tr>';
 	if (!empty($resultado_detalle)) {
        $contador = 0;
        $base = 0;
        foreach ($resultado_detalle as $valor) {
           
            $subtotal = number_format(($valor['importe']*$valor['cantidad'])-(($valor['importe']*$valor['cantidad'])*($valor['dto']/100)),2,'.','');
            $base +=  $subtotal;
            $contador ++;
            $contenido .= '<tr  class="row-det">
			<td>'.$valor['codigo'].'</td>
			<td colspan="2">'.$valor['desc_repuesto'].'</td>
			<td align="center">'.$valor['dto'].'</td>
			<td align="center">'.$valor['cantidad'].'</td>
			<td align="right">'.$valor['importe'].'</td>
			<td align="right">'.$subtotal.'</td>
		</tr>';
            
            }
    }
          $dto_pie = number_format(($base*$dto)/100,2,'.','');
          $subtotal_pie = $base - $dto_pie;
          $iva_pie =  number_format(($subtotal_pie * $iva/100),2,'.','');
          $total = number_format(($subtotal_pie + $iva_pie),2,'.','');
          
          for ($i=$contador;$i<17;$i++){
               $contenido .= '<tr class="row-tr">
			<td></td>
			<td colspan="2"></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			</tr>';
          }      
$contenido .= 		
		'<tr>
			<td colspan="4" valign="top"><b>Fecha y Firma</b></td>
			<td colspan="3">
				<p><b>BASE: </b><span align="right">'.number_format($base,2,'.','').' €</span></p>
				<p><b>DTO %: </b>'.$dto_pie.' €</p>
				<p><b>SUBTOTAL: </b>'.$subtotal_pie.' €</p>
				<p><b>I.V.A. 21%: </b>'.$iva_pie.' €</p>
				<p><b>TOTAL: </b>'.$total.' €</p>
			</td>
		</tr>	
	</table>
        <p style="text-align:justify;font-size:8px">José Isidoro Vidal es el Responsable del tratamiento de los datos personales proporcionados bajo su 
        consentimiento y le informa que estos datos serán tratados de conformidad con lo dispuesto en el 
        Reglamento (UE) 2016/679 de 27 de abril de 2016 (GDPR), con la finalidad de mantener una relación comercial
        y conservarlos mientras exista un interés mutuo para mantener el fin del tratamiento y cuando ya no sea 
        necesario para tal fin, se suprimirán con medidas de seguridad adecuadas para garantizar la seudonimización
        de los datos o la destrucción total de los mismos. No se comunicarán los datos a terceros, salvo obligación
        legal. Asimismo, se informa que puede ejercer los derechos de acceso, rectificación, portabilidad y 
        supresión de sus datos y los de limitación y oposición a su tratamiento dirigiéndose a José Isidoro Vidal
        en Bº La Trapa, S/N, 39718, San Vitores, Medio Cudeyo(Cantabria)</p>
</body>
</html>';
}
               
        $mpdf = NEW Mpdf([
            'mode' => 'utf-8',
//            "format" => "A4-L",
    //           
//                "margin_bottom" => 3,
//                "margin_top" => 3,
    //            "margin_footer" => 0
        ]);
        
        $css = file_get_contents('../web/css/factura.css');
     
        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($contenido, \Mpdf\HTMLParserMode::HTML_BODY);
//        $mpdf->AddPage();
//        $mpdf->WriteHTML($css, \Mpdf\HTMLParserMode::HEADER_CSS);
//        $mpdf->WriteHTML($contenido_pag2,\Mpdf\HTMLParserMode::HTML_BODY); 
            $mpdf->Output(); 
           
               
    }
    
    
     public function actionExportar_excel(){
     
//         $fecha_inicio = date( "Y-m-d", strtotime( "2021-02-19" ));
//         $fecha_fin =  new DateTime("2021-02-16");
//         echo"<pre>";
//          var_dump(Yii::$app->request->post('FiltroExporta'));
//         var_dump(Yii::$app->request->post());
//                       echo"</pre>";
            $model = new FiltroExporta();
           
           
            //var_dump(Yii::$app->request->queryParams);
            
       
          //if ($model->load(Yii::$app->request->post())) {
                //$fecha_inicio = date('Y-m-d',Yii::$app->request->post('fechaInicio'));
               // var_dump($fecha_inicio);
                //$fecha_inicio = Yii::$app->formatter->asDate(Yii::$app->request->post('fechaInicio'), 'php:Y-m-d');
                        //date( "Y-m-d", strtotime( Yii::$app->request->post('fechaInicio') ));
              
              if(Yii::$app->request->post('FiltroExporta') !== null){
                $fecha_inicio = date( "Y-m-d", strtotime(Yii::$app->request->post('FiltroExporta')['fechaInicio']));
                $fecha_fin = date( "Y-m-d", strtotime(Yii::$app->request->post('FiltroExporta')['fechaFin']));
                $estado = Yii::$app->request->post('FiltroExporta')['estado'];
                $concepto = Yii::$app->request->post('FiltroExporta')['concepto'];
               
              }else{
                    $fecha_inicio = "01-01-2021";
                    $fecha_fin = "31-12-2021";
                    $estado = "";
                    $concepto = "";
                 
              }  
              if($concepto != Null){
                   $filtro_concepto = " AND p2.codigo = '$concepto'";
              }else{
                  $filtro_concepto = "";  
              }
          //}
          //$model = new Facturas();
           $query = new SqlDataProvider([
                'sql' => "SELECT f.parte parte,f.factura factura,f.fecha fecha,f.tipoiva tipoiva,f.iva iva,f.subtotal subtotal,
                                 f.total total,p1.estado estado, p2.codigo codigo
                          FROM facturas f 
                                     join parte1 p1 on f.parte = p1.id 
                                     join parte2 p2 on p1.id = p2.id_parte1
                          WHERE f.fecha between '$fecha_inicio' AND '$fecha_fin' AND p1.estado = $estado $filtro_concepto
                          GROUP BY f.factura"   
           ]); 
            $dataProvider = $query->getModels();  
              
              
            
//          $dataProvider = Facturas::find()
//                  ->leftJoin('parte1', 'parte1.nparte = facturas.parte')
//                  ->leftJoin('parte2', 'parte2.id_parte1 = parte1.id')
//                  ->select(['facturas.parte parte','facturas.factura factura','facturas.fecha fecha','facturas.tipoiva tipoiva','facturas.iva iva','facturas.subtotal subtotal','facturas.total total','parte1.estado estado','parte2.codigo codigo'])
//                  ->where(['between', 'fecha', $fecha_inicio, $fecha_fin])
//                  ->andWhere(['parte1.estado' => $estado])
//                  ->all()
                   // ($concepto != Null or $concepto != '') ? "->andWhere(['parte1.estado' => $estado]);":
//                        ";";
     
                  
           // $searchModel = new FacturasSearch();
           // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//            $dataProvider->query->where(['between', 'fecha', $fecha_inicio, $fecha_fin]);   
//            $dataProvider->query->andWhere(['']);
            
            //calculamos el total de la columna total
            $total = 0;
            if (!empty($dataProvider)) {
                foreach ($dataProvider as $val) {
                    $total += $val['total'];
                }
            }
         
//            $dataProvider = new ActiveDataProvider([
//                'query' => $query,
//                'pagination' => [
//                    'pageSize' => 10,
//                ],
//              
//            ]);
            //$dataProvider = $searchModel->search(Yii::$app->request->queryParams($query));
//         $dataProvider = $searchModel->search(Yii::$app->request->post('FiltroExporta'));
        // $dataProvider = $searchModel->search(Yii::$app->request->post());
         
     $gridColumns = [//['class' => 'yii\grid\SerialColumn'],
                           //'id',
                           'parte',
                           'factura',
                           'fecha',
                            [
                            'attribute'=>'tipoiva',
                            'headerOptions' => ['style' => 'text-align: center !important;'],
                            'contentOptions' => ['style' => 'text-align: center !important;'],  
                            ],
                            [
                            'attribute'=>'iva',
                            'headerOptions' => ['style' => 'text-align: center !important;'],
                            'contentOptions' => ['style' => 'text-align: right !important;'],  
                            ],
                            [
                            'attribute'=>'subtotal',
                            'headerOptions' => ['style' => 'text-align: center !important;'],
                            'contentOptions' => ['style' => 'text-align: right !important;'],   
                            ],
                            [
                            'attribute'=>'total',
                            'footer'=>$total,
                            'headerOptions' => ['style' => 'text-align: center !important;'],
                            'footerOptions' => ['style' => 'text-align: right !important;'],   
                            'contentOptions' => ['style' => 'text-align: right !important;'],
                                
                            ],
                           ['class' => 'yii\grid\ActionColumn'],
                       ];

          $model->fechaInicio = $fecha_inicio;
          $model->fechaFin = $fecha_fin;
          $model->estado = $estado;
          $model->concepto = $concepto;
            
            return $this->render('informes', [
            'model' => $model,
           // 'searchModel' => $searchModel,
            'dataProvider' => $query,
            'gridColumns' => $gridColumns,
            'total' => $total,
        ]);
         
         
       
         
     }
    
}
