<?php

namespace app\controllers;

use Yii;
use app\models\Vehiculos;
use app\models\VehiculosSearch;
use app\models\Clientes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/**
 * VehiculosController implements the CRUD actions for Vehiculos model.
 */
class VehiculosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vehiculos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VehiculosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vehiculos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vehiculos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vehiculos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vehiculos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
     public function actionUpdatemodal($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->renderPartial('update', [
            'model' => $model,
        ]);
    }
    

    /**
     * Deletes an existing Vehiculos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        
    //comprobamos si tiene algun parte de reparacion asignado
    $model_comprueba = new vehiculos();
    if($model_comprueba->getTiene_partes($id)){
        $mensaje = 'El Vehículo no se puede eliminar por que tiene partes de reparación asignados.';
        return $this->redirect(array('index?mensaje='.$mensaje));     
        //return $this->redirect(['index']);
    }
        
        $this->findModel($id)->delete();
//        if (Yii::$app->request->isAjax) {
//            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//            return ['success' => true];
//        }
        return $this->redirect(['index']);
//       if (Yii::$app->request->post('modo') == 'pjax')
            
    }

    /**
     * Finds the Vehiculos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vehiculos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vehiculos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
     public function actionVehiculos_cliente()
    {
       
         
         $model = new vehiculos();
         
         if (Yii::$app->request->post('matricula')){
             
            $cliente = Yii::$app->request->post('cliente');
            $matricula = Yii::$app->request->post('matricula');
            $marca = Yii::$app->request->post('marca');
            $color = Yii::$app->request->post('color');
            $bastidor = Yii::$app->request->post('bastidor');
            $combustible = Yii::$app->request->post('combustible');
            
            $model->cliente = $cliente;
            $model->matricula = $matricula;
            $model->marca = $marca;
            $model->color = $color;
            $model->bastidor = $bastidor;
            $model->combustible = $combustible;
            
//            $model_cliente = new clientes();
//            $model_cliente->find()->where(['id' => $cliente]);
            
                                
          
            
            
            if(!$model->getDuplicada($matricula)){
                $model->save();
               
                $mensaje = "Vehículo Guardado";
                 
            }else{
                $mensaje = "Matricula ya existe";
             
            }
            
             $consulta = $model->find()
                ->where(['cliente'=>$cliente])
            //->andFilterWhere(['like', 'apellidos', $texto_buscar])
                ->orderBy(['id' => SORT_DESC]);
            
            $dataProvider = new ActiveDataProvider([
                'query' => $consulta,
                'pagination' => false,
            ]);               
                  
            return $mensaje;
        }
    }
    public function actionEliminar_pjax($id)
    {
        if (Yii::$app->request->post('cliente')){
            $cliente = Yii::$app->request->post('cliente');
            //comprobamos si tiene algun parte de reparacion asignado
            $model_comprueba = new vehiculos();
            if($model_comprueba->getTiene_partes($id)){
                $mensaje = 'El Vehículo no se puede eliminar por que tiene partes de reparación asignados.';
                return $this->redirect(array('clientes/update?id='.$cliente.'&mensaje='.$mensaje));         
            }
                           
            $this->findModel($id)->delete();
            return $this->redirect(['clientes/update?id='.$cliente]);
        }
    }
     public function actionDatos_cabecera($vehiculo = Null)
    {
        
       
        if (isset($vehiculo)){
            $consulta = vehiculos::find()
                ->from('vehiculos v')   
                ->select([
                    'nombre'=>'c.nombre',
                    'apellidos'=>'c.apellidos',
                    'localidad'=>'c.localidad',
                    'cif'=>'c.cif_nif',
                    'matricula'=>'v.matricula',
                    'marca'=>'v.marca',
                    'bastidor'=>'v.bastidor',
                     ])
                 ->innerJoin('clientes c', 'c.id = v.cliente')
                 ->where(['v.id'=>$vehiculo])
                ->asArray()
                ->all();
         
             //Yii::$app->response->format = Response::FORMAT_JSON;
            return json_encode($consulta);
           
        }
    }
    
}