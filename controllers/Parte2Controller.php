<?php

namespace app\controllers;

use Yii;
use app\models\Parte2;
use app\models\Parte2Search;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Parte2Controller implements the CRUD actions for Parte2 model.
 */
class Parte2Controller extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Parte2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Parte2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Parte2 model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Parte2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Parte2();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Parte2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
     public function actionUpdatemodal($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        return $this->renderPartial('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Parte2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Parte2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Parte2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parte2::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionDetalle_parte(){
        
        $model = new Parte2();
         
         if (Yii::$app->request->post('idparte')){
          
            $parte = Yii::$app->request->post('idparte');
           	$codigo_detalle = Yii::$app->request->post('codigo_detalle');          
            $repuesto = Yii::$app->request->post('repuesto');
            $descripcion = Yii::$app->request->post('descripcion');
            $cantidad = Yii::$app->request->post('cantidad');
            $dto = Yii::$app->request->post('dto');
            $importe = Yii::$app->request->post('importe');
            
            $model->id_parte1 = $parte;
            $model->codigo = $repuesto;
            $model->descripcion = $descripcion;
            $model->cantidad = $cantidad;
            $model->dto = $dto;
            $model->importe = $importe;
           
            $model_actualiza = new Parte2();
           if($codigo_detalle != ''){
                if($registro_actualizar = $model_actualiza::find()-where(['id'=>$codigo_detalle])->one()){
                      $registro_actualizar->codigo = $repuesto;
                      $registro_actualizar->descripcion = $descripcion;
                      $registro_actualizar->cantidad = $cantidad;
                      $registro_actualizar->dto = $dto;
                      $registro_actualizar->importe = $importe;
                      $registro_actualizar->update();
                }  
            }else{  
              $model->save();
            } 
               
            
              $consulta = $model->find()
                ->select([
                    'codigo'=>'codigo',
                    'descripcion' => 'descripcion',
                    'cantidad' => 'cantidad',
                    'descuento' => 'descuento',
                    'importe' => 'importe',
                  
                ])
                ->where(['id_parte1'=>$parte])
            //->andFilterWhere(['like', 'apellidos', $texto_buscar])
                ->orderBy(['id' => SORT_ASC]);
            
            $dataProvider = new ActiveDataProvider([
                'query' => $consulta,
                'pagination' => false,
            ]);               
        }
    }
    public function actionEliminar_pjax($id)
    {
    
        if (Yii::$app->request->post('parte')){
           
            $parte = Yii::$app->request->post('parte');
         
            $this->findModel($id)->delete();
            return $this->redirect(['parte1/update?id='.$parte]);
        }
    }
}
