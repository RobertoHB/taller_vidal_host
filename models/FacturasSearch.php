<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Facturas;

/**
 * FacturasSearch represents the model behind the search form of `app\models\Facturas`.
 */
class FacturasSearch extends Facturas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parte', 'factura', 'tipoiva'], 'integer'],
            [['fecha'], 'safe'],
            [['iva', 'subtotal', 'total'], 'number'],
            //[['comentarios'], 'string'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Facturas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parte' => $this->parte,
            'factura' => $this->factura,
            'fecha' => $this->fecha,
            'tipoiva' => $this->tipoiva,
            'iva' => $this->iva,
            'subtotal' => $this->subtotal,
            'total' => $this->total,
        ]);

        return $dataProvider;
    }
}
