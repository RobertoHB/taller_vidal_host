<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "facturas".
 *
 * @property int $id
 * @property int|null $parte
 * @property int|null $factura
 * @property string|null $fecha
 * @property int|null $tipoiva
 * @property float|null $iva
 * @property float|null $subtotal
 * @property float|null $total
 *
 * @property Parte1 $parte0
 */
class Facturas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'facturas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parte', 'factura', 'tipoiva'], 'integer'],
            [['fecha'], 'safe'],
            [['iva', 'subtotal', 'total'], 'number'],
            [['comentarios'], 'string'],
            [['parte'], 'exist', 'skipOnError' => true, 'targetClass' => Parte1::className(), 'targetAttribute' => ['parte' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parte' => 'Parte',
            'factura' => 'Factura',
            'fecha' => 'Fecha',
            'tipoiva' => 'Tipoiva',
            'comentarios' => 'Comentarios',
            'iva' => 'Iva',
            'subtotal' => 'Subtotal',
            'total' => 'Total',
        ];
    }

    /**
     * Gets query for [[Parte0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParte0()
    {
        return $this->hasOne(Parte1::className(), ['id' => 'parte']);
    }
   public function getPartes()
    {
    return $this->hasMany(Parte1::className(), ['id' => 'parte']);
}

    public function getVehiculo0()
    {
        return $this->hasMany(Vehiculos::className(), ['id' => 'vehiculo'])
                ->viaTable('{{%parte1}}', ['vehiculo' => 'id']);
    }
    
        public function getCliente()
    {
        $query = UserComment::find();
        $query->leftJoin('user_comment_user',      'user_comment_user.user_comment_id=user_comment.id');
        $query->where(['receiver_id'=>$this->id, 'sender_id'=>$id]);
        $query->orWhere(['receiver_id'=>$id, 'sender_id'=>$this->id]);
        $query->orderBy(['created_at'=>SORT_DESC]);

        return $query;
    }
    
    public function afterFind() {
     parent::afterFind();
      if($this->fecha != Null){
         $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
      }   
    }
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        //$this->entrada=Yii::$app->formatter->asDatetime($this->entrada, 'php:Y-m-d H:i:s');
        //$this->salida=Yii::$app->formatter->asDatetime($this->salida, 'php:Y-m-d H:i:s');


         if($this->fecha != Null){
          $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d');

        return parent::beforeSave($insert);

      }
    }
    
}
