<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "repuestos".
 *
 * @property int $id
 * @property string|null $referencia
 * @property string|null $descripcion
 * @property string|null $proveedor
 * @property string|null $marca
 */
class Repuestos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repuestos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['referencia', 'marca'], 'string', 'max' => 25],
            [['descripcion'], 'string', 'max' => 300],
            [['proveedor'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'referencia' => 'Referencia',
            'descripcion' => 'Descripcion',
            'proveedor' => 'Proveedor',
            'marca' => 'Marca',
        ];
    }
    public function getUltimoRepuesto(){
         $ultimo = Repuestos::find()
                        ->max('id');  
         return $ultimo;
    }
}
