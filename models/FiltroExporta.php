<?php

namespace app\models;


use Yii;
use app\models\Facturas;

 
 
class FiltroExporta extends Facturas
{
    public $fechaInicio;
    public $fechaFin;
    public $estado;
    public $concepto;
    /**
     * {@inheritdoc}
     */
  

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaInicio','date'],'required'],
            [['fechaFin','date'],'required'],
            [['estado', 'integer'],'required'],
            //[['concepto', 'string']],  
       ];        
    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//         $labels = ['fechaInicio' => 'Fecha Inicio', 'fechaFin' => 'Fecha Fin', 'estado' => 'Estado', 'concepto' => 'Concepto'];
//        return array_merge(parent::attributeLabels(), $labels);
//       
//    }
    
}