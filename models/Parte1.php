<?php

namespace app\models;

use Yii;

use app\models\Clientes;
use app\models\Repuestos;
use NumberFormatter;
use yii\helpers\Json;
use yii\web\Response;
use yii\db\Expression;
/**
 * This is the model class for table "parte1".
 *
 * @property int $id
 * @property int|null $vehiculo
 * @property string|null $entrada
 * @property string|null $salida
 * @property int|null $nparte
 * @property int|null $nfactura
 * @property string|null $descripcion
 * @property int|null $estado
 * @property int|null $kms
 * @property int|null $dto
 * @property int|null $iva
 *
 * @property Vehiculos $vehiculo0
 * @property Parte2[] $parte2s
 */
class parte1 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parte1';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vehiculo', 'nparte', 'nfactura', 'estado', 'dto', 'iva'], 'integer'],
            [['entrada', 'salida','fecha_presupuesto'], 'safe'],
//            ['entrada', 'datetime', 'format' => 'php:Y-m-d H:i:s'],
//            ['salida', 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            //[['entrada','salida'], 'datetime', 'format' => 'php:y-m-d'],
            [['descripcion','kms'], 'string'],
            [['vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculos::className(), 'targetAttribute' => ['vehiculo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vehiculo' => 'Vehiculo',
            'fecha_presupuesto' => 'Fecha',
            'entrada' => 'Entrada',
            'salida' => 'Salida',
            'nparte' => 'Nparte',
            'nfactura' => 'Nfactura',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
            'kms' => 'Kms',
            'dto' => 'Dto',
            'iva' => 'Iva',
        ];
    }

    /**
     * Gets query for [[Vehiculo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo0()
    {
        return $this->hasOne(Vehiculos::className(), ['id' => 'vehiculo']);
    }
    public function getCliente0()
    {
                  
        return $this->hasOne(Clientes::className(), ['id' => 'vehiculos.cliente'])
            ->viaTable('vehiculos', ['id' => 'vehiculo']);
            
            //->orderBy(['sort' => SORT_DESC]
            
                
                
       
//        return (clientes::find()
//                ->from('clientes c')   
//                ->select([
//                    'nombre'=>'c.nombre',
//                    'apellidos'=>'c.apellidos',
//                     ])
//                 ->innerJoin('vehiculos v', 'c.id = v.cliente')
//                ->innerJoin('parte1 p', 'p.vehiculo = v.id')
//                
//               );
    }
     public function getVehiculos_autocomplete()
    {
        $consulta = clientes::find()
            ->select(['v.id as id','concat(c.nombre," ",c.apellidos," ",v.matricula," ",v.marca) as value','concat(c.nombre," ",c.apellidos," ",v.matricula," ",v.marca) as label'])
            ->from('clientes c')   
            ->innerJoin('vehiculos v', 'c.id = v.cliente')
            ->asArray()
            ->all();
        //Yii::$app->response->format = Response::FORMAT_JSON;
//        $data_obj = json_encode($consulta);
//        $datos = get_object_vars($data_obj);
//       foreach ($datos as  $campo=>$valor) {
//          
//            $arr_datos[] = $valor;
//        }
//        $filtered_array = array_filter($consulta, function($val) {
//         return ($val['nombre'].$val['apellidos'].$val['marca'].$val['marca']);
           //});
//           return $filtered_array;
        //return array_columns($consulta, 'nombre','apellidos');
        //return Json_encode($consulta);
        return $consulta;
        //return $arr_datos;
    }
      public function getRepuestos_autocomplete()
    {
        $consulta = repuestos::find()
            ->select(['referencia','descripcion','concat(referencia," ",descripcion) as value','concat(referencia," ",descripcion) as label'])
            ->from('repuestos')   
            ->asArray()
            ->all();
        //Yii::$app->response->format = Response::FORMAT_JSON;
//        $data_obj = json_encode($consulta);
//        $datos = get_object_vars($data_obj);
//       foreach ($datos as  $campo=>$valor) {
//          
//            $arr_datos[] = $valor;
//        }
//        $filtered_array = array_filter($consulta, function($val) {
//         return ($val['nombre'].$val['apellidos'].$val['marca'].$val['marca']);
           //});
//           return $filtered_array;
        //return array_columns($consulta, 'nombre','apellidos');
        //return Json_encode($consulta);
        return $consulta;
        //return $arr_datos;
    }
        public function getFacturacion()
    {
        $mes_actual = date('m');
        $consulta = \app\models\Facturas::find()
            ->select(['total'=> 'sum(total)'])
            ->from('facturas')
            ->where(['month(fecha)'=> $mes_actual])
            ->asArray()
            ->all();
          
          return($consulta[0]['total']);
    }
        public function getUltimaFactura()
    {
       
        $consulta = \app\models\Facturas::find()
              ->select(['ultimafact'=> 'max(factura)'])
            ->from('facturas')
            ->asArray()
            ->all();
          
          return($consulta[0]['ultimafact']);
    }
    /**
     * Gets query for [[Parte2s]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParte2s()
    {
        return $this->hasMany(Parte2::className(), ['id_parte1' => 'id']);
    }
    
    
    public function afterFind() {
        parent::afterFind();
//        if ($this->iva != Null)
//            $this->iva= number_format($this->iva,0,',','.').' %';
//          $this->iva=Yii::$app->formatter->asCurrency($this->iva, '%',[number_format($this->iva,$demimals='0')]);
//        if ($this->dto != Null)
//             $this->dto= number_format($this->dto,0,',','.').' %';
//          $this->dto=Yii::$app->formatter->asCurrency($this->dto,'%', [NumberFormatter::MAX_SIGNIFICANT_DIGITS=>100]);
         if ($this->kms != Null){
           // $this->kms = intval(str_replace(".", "",$this->kms));
             $kms_int = intval($this->kms);
             $kms_millar = number_format($kms_int,0,',','.').' Kms';
             $this->kms= $kms_millar;
    }
//          $this->kms=Yii::$app->formatter->asCurrency($this->kms, 'kms', [NumberFormatter::MAX_SIGNIFICANT_DIGITS=>100]);
//        $this->iva = \yii\i18n\Formatter::asPercent($this->iva,1.0,0);
            
          
        //$this->entrada=Yii::$app->formatter->asDatetime($this->entrada, 'php:d-m-Y H:i');
        if($this->fecha_presupuesto != Null){
            $this->fecha_presupuesto=Yii::$app->formatter->asDate($this->fecha_presupuesto, 'php:d-m-Y');
        }else{
            $this->fecha_presupuesto = Null; 
        }
       // $this->fecha_presupuesto=Yii::$app->formatter->asDate($this->fecha_presupuesto, 'php:d-m-Y');
        
         if($this->entrada != Null){
            $this->entrada=Yii::$app->formatter->asDate($this->entrada, 'php:d-m-Y');
            if($this->salida != Null){
                 $this->salida=Yii::$app->formatter->asDate($this->salida, 'php:d-m-Y');
            }else{
                $this->salida = Null; 
             }
          }else{
              $this->entrada = Null;         
          }  

    }
   
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          //$this->entrada=Yii::$app->formatter->asDatetime($this->entrada, 'php:Y-m-d H:i:s');
          $this->fecha_presupuesto=Yii::$app->formatter->asDate($this->fecha_presupuesto, 'php:Y-m-d');
          
          $kms_1= str_replace(" kms", "", $this->kms);
           $kms_2= str_replace(".", "", $kms_1);
//               $this->kms = trim($this->kms);
//              $this->kms =str_replace(' ', '', $this->kms);
//            $this->dto = str_replace(" %", "", $this->dto);
//            $this->iva = str_replace(" %", "", $this->iva);
         
            $this->kms =  $kms_2;
//            $this->iva = (int) $this->iva;
//            $this->dto = (int) $this->dto;
          
           if($this->entrada != Null){
            $this->entrada=Yii::$app->formatter->asDate($this->entrada, 'php:Y-m-d');
            if($this->salida != Null){
                 $this->salida=Yii::$app->formatter->asDate($this->salida, 'php:Y-m-d');
            }else{
                $this->salida = Null; 
             }
          }else{
              $this->entrada = Null;
             
          }
          return parent::beforeSave($insert);
         
          //$this->alta= \DateTime::createFromFormat("d/m/Y", $this->alta)->format("Y/m/d");
          //return true;
    }
}
