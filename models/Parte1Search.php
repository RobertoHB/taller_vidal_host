<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Parte1;

/**
 * Parte1Search represents the model behind the search form of `app\models\Parte1`.
 */
class Parte1Search extends Parte1
{
   
    public $filtro_matricula;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vehiculo', 'nparte', 'nfactura', 'estado', 'kms', 'dto', 'iva'], 'integer'],
            [['entrada', 'salida','fecha_presupuesto', 'descripcion','filtro_matricula'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Parte1::find()
                ->joinWith(['vehiculo0']);
              

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

       $dataProvider->setSort([
        'attributes' => [
            'id',
            'vehiculo',
            'filtro_matricula',
            'nparte',
            'nfactura',
            'estado',
            'kms',
            'dto',
            'iva',
          	'fecha_presupuesto',
            'entrada',
            'salida',
            'descripcion',
            ]
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vehiculo' => $this->vehiculo,
           	'fecha_presupuesto' => $this->fecha_presupuesto,
            'entrada' => $this->entrada,
            'salida' => $this->salida,
            'nparte' => $this->nparte,
            'nfactura' => $this->nfactura,
            'estado' => $this->estado,
            'kms' => $this->kms,
            'dto' => $this->dto,
            'iva' => $this->iva,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion])
                ->andFilterWhere(['like', 'vehiculos.matricula', $this->filtro_matricula]);
               
        return $dataProvider;
    }
}
