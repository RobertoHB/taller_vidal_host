<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\Vehiculos;
/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = 'Cliente: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';

if(isset($_REQUEST['mensaje'])){
    $msg = $_REQUEST['mensaje'];
}else{
   $msg = ""; 
}

$vehiculo = new vehiculos();                        
$consulta = $vehiculo->find()
->where(['cliente'=>$model->id])
//->andFilterWhere(['like', 'apellidos', $texto_buscar])
->orderBy(['id' => SORT_DESC]);
           



$dataProvider = new ActiveDataProvider([
    'query' => $consulta,
    'pagination' => false,
]);

?>
<div class="clientes-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
        'mensaje' => $msg
    ]) ?>

</div>
