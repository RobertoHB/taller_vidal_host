<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
if(isset($_REQUEST['mensaje'])){
    $mensaje = $_REQUEST['mensaje'];
}else{
    $mensaje ="";    
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<div class="clientes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Cliente', ['create'], ['class' => 'btn btn-success','style'=>'background-color: #2395D1']) ?>
        <span id ="msg" style="margin-left:200px;background-color: #2191c0;color:white;font-size:20px;border:solid 1px #EEE;border-radius:15px;padding:5px;display:none"><?=$mensaje?></span>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellidos',
            'rs',
            'cif_nif',
            //'direccion',
            //'cp',
            //'localidad',
            //'provincia',
            //'email:email',
            //'fijo',
            //'movil',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',
                 'contentOptions'=>[ 'style'=>'width: 100px;font-size:20px'], 
                ],
        ],
    ]); ?>


</div>


<script>
$( document ).ready(function() {
    
if($('#msg').html()!= ''){
     $("#msg").show();
   for(let i=0;i<5;i++){
            $("#msg").fadeToggle(1700);    
        }   
    $("#msg").fadeOut(); 
}     

    
});
</script>
