<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
$tiposcombustible = ['Gasolina' => 'Gasolina', 'Diesel' => 'Diesel','Gas Natural' => 'Gas Natural','Bioetanol' => 'Bioetanol','Bioediesel' => 'Biodiesel','Biogás' => 'Biogás','Eléctrico' => 'Eléctrico','Hidrógeno' => 'Hidrógeno','Solar' => 'Solar',]; 
if(isset($_REQUEST['mensaje'])){
    $mensaje = $_REQUEST['mensaje'];
}else{
    $mensaje = '';
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<div class="clientes-form">

    <?php $form = ActiveForm::begin(); ?>
     <?= $form->field($model, 'id')->hiddenInput(['maxlength' => true,'id'=>'codigo_cliente'])->label('') ?>
    <div class="container" style="margin-top:-25px;">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                 <?= $form->field($model, 'rs')->textInput(['maxlength' => true]) ?>
            </div>
        </div>    
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'cif_nif')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
               <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'cp')->textInput() ?>
            </div>    
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'localidad')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
               <?= $form->field($model, 'provincia')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>    
        </div>
        <div class="row">
            <div class="col-sm-4">
                   <?= $form->field($model, 'fijo')->textInput() ?>  
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'movil')->textInput() ?>
            </div>
            <div class="col-sm-4">
                
            </div>    
        </div>

    </div>


    <div class="form-group">
      
         <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'envioForm']) ?>
    </div>
    <span style="margin-left:250px;background-color: #2191c0;color:white;font-size:20px;border:solid 1px #EEE;border-radius:15px;padding:5px;display:none" id="msjs_form"><?= $mensaje ?></span>

    <?php ActiveForm::end(); ?>

</div>

<hr>
<h3>Vehiculos</h3>


<!-- Html::beginForm([
        'vehiculos/vehiculos_cliente'], 
        'post', 
        ['data-pjax' => '', 'class' => 'form-inline']
   ); -->
<div class="form-inline" id="form_vehiculos">
    <div class="row">
        <div class="col-xs-10 col-sm-11">
            <p id='msjs_form' style='text-align: center;background-color: #F5F5F5;width: 500px;margin-left: 300px;font-weight: bold;border-radius: 40px;'></p>
            <?= Html::input('text', 'cliente', Yii::$app->request->post('cliente'), ['class' => 'form-control','id'=> 'cliente_vehiculo','style'=>'display:none']) ?>
            <?= Html::input('text', 'matricula', Yii::$app->request->post('matricula'), ['class' => 'form-control','placeholder'=>'Matricula','id'=> 'matricula']) ?>
            <?= Html::input('text', 'marca', Yii::$app->request->post('marca'), ['class' => 'form-control','placeholder'=>'Marca','id'=> 'marca']) ?>
            <?= Html::input('text', 'color', Yii::$app->request->post('color'), ['class' => 'form-control','placeholder'=>'Color','id'=> 'color']) ?>
            <?= Html::input('text', 'bastidor', Yii::$app->request->post('bastidor'), ['class' => 'form-control','placeholder'=>'Bastidor','id'=> 'bastidor']) ?>
            <?= Html::dropDownList('combustible',Yii::$app->request->post('bastidor'), $tiposcombustible, ['class' => 'form-control','placeholder'=>'Combustible','id'=> 'combustible']) ?>
        </div>
        <div class="col-xs-2 col-sm-1" style="margin-top:8px;">
            <?= Html::Button('Añadir', ['class' => 'btn btn-primary', 'name' => 'buscar_vehiculos','id' => 'buscar_vehiculos','style'=>'width:100px;margin-left:-20px;']) ?>
        </div>   
    </div>
</div>
<!--Html::endForm()-->

<div id="mygrid" style="margin-top: 15px;">
<?php Pjax::begin(['id'=>'vehiculos-pjax']); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
//        'id' => 'myGrid',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'cliente'
            'matricula',
            'marca',
            'color',
            'bastidor',
            'combustible',
           
            ['class' => 'yii\grid\ActionColumn',
                
            'contentOptions' => ['style' => 'width:80px;'],
            'header'=>'Opciones',
            'template' => '{update}{delete}',
            'buttons' => [
                             'update' => function ($url,$model,$key) {
                                                return Html::button(
                                                '<span class="glyphicon glyphicon-pencil"></span>',['value' =>url::to('/web/index.php/vehiculos/updatemodal'.'?id='.$model->id),'id'=>'modalBtnUpdate','style'=>'border:none;background-color:transparent;font-size:20px;']);
                                                       
//                                                    ['data'=>[
//                                                                'method' => 'get',
//                                                                'params'=>['id'=>$model->id],                                         
//                                                                ]
//                                                    ]
//                                                        );
                                },
                            'delete' => function ($url, $model, $key) {
                                return  Html::a(
                                '<span class="glyphicon glyphicon-trash" style="padding-left:5px;;font-size:20px;"></span>',Url::to('/web/index.php/vehiculos/eliminar_pjax'.'?id='.$model->id),                                                                
                                                         ['data'=>[
                                                                'method' => 'post',
                                                                'confirm' => 'Desea eliminar?',
                                                                'params'=>['cliente'=>$model->cliente],
                                                                ]
                                                        ]);
  
                            }
                        ]
            ],  

        ],
    ]); ?>
<?php Pjax::end(); ?>
    
</div>
 <!--ventana modal para actualizar vehiculos-->
<div id="modal_actualizar_vehiculos">
    <?php Modal::begin([
        'id'=>'modal_act_vehiculos',
        'size'=>'modal-lg',
        'header' => '<h2>Actualizacion de Vehiculo </h2>',
            //'toggleButton' => ['label' => 'click me'],
    ]);

        echo "<div id='modalContent'></div>";

    Modal::end();?>
</div>


<script>
$( document ).ready(function() {
//     console.log( $('#codigo_cliente').html());
  
    if ( $('#codigo_cliente').val() != '' ){  
        $('#cliente_vehiculo').val($('#codigo_cliente').val());  
    }
    
    if($('#msjs_form').html() != ''){
        $("#msjs_form").show();
        for(let i=0;i<3;i++){
            $("#msjs_form").fadeToggle(1500);    
        }   
         $("#msjs_form").fadeOut(); 
    }     
    
    
    $(document).on('pjax:error', function(event, xhr) {
         $('#mensaje').html('Datos introducidos erroneos o matricula duplicada');  
//        alert('Pjax failed!');
//        console.log(xhr.responseText);
//        event.preventDefault();
    });
    
//    $("#vehiculos-pjax").on("pjax:end", function() {
//        $.pjax.reload({container:"#vehiculos-pjax"});
//        $('#mensaje').html('Datos vehiculo guardados');  //Reload GridView
//    });
//    
    
    
      $("#buscar_vehiculos").click(function(){
          actualizarVehiculos();
      });
      
      
      $(document).on("click", "#modalBtnUpdate", function () {
        //console.log("mostrar modal")
         $('#modal_act_vehiculos').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
     });
      
      
      //controlamos en el formulario de introduccion de vehiculos que no se introducen matriculas de mas de 10 caracteres ni
      //numeros de bastidor de más de 17
      
    $('#matricula').keypress(function () {
        var maxLength = $(this).val().length;
        if (maxLength > 9) {
            var texto = 'No puedes introducir mas de ' + maxLength + ' caracteres';
            mostrarMensaje(texto);
            return false;
        }
    });
    $('#bastidor').keypress(function () {
    var maxLength = $(this).val().length;
        if (maxLength > 16) {
            var texto = 'No puedes introducir mas de ' + maxLength + ' caracteres';
            mostrarMensaje(texto);
            return false;
        }
    });
      
      
//       if($('#envioForm').hasClass("btn btn-primary")){
//               
//        }     
        if($('#envioForm').hasClass("btn btn-success")){
       
            $('#form_vehiculos div').children().prop('disabled', true);
        }      
//       $("#btn_eliminar_vehiculo").click(function(){
//          var vehiculo = $(this).attr("value");
//         
//           eliminarVehiculos(vehiculo);       
//         
//      });
      
      
      
}); 
    function mostrarMensaje(texto){
        
    $("#msjs_form").text(texto);
        $("#msjs_form").show();
       for(let i=0;i<3;i++){
            $("#msjs_form").fadeToggle(1700);    
        }   
        $("#msjs_form").fadeOut();
    }

    function actualizarVehiculos(){    
        
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

              type: "POST",
              url: baseUrl + "vehiculos/vehiculos_cliente",

              data: {
                cliente: $("#cliente_vehiculo").val(),
                matricula: $("#matricula").val(),
                marca: $("#marca").val(),
                color: $("#color").val(),
                bastidor: $("#bastidor").val(),
                combustible: $("#combustible").val(),

              }

          })
               .done(function (data) {
                     mostrarMensaje(data);
                    $.pjax.reload({container:"#vehiculos-pjax",timeout: false});
                    $.pjax.reload({container:"#mygrid",timeout: false});
                    
                     
                     
                    //$("#mygrid").load("#mygrid");
                   
                   
        
                   // $( "#mensaje" ).html( data );
          });
    }
//      function eliminarVehiculos(vehiculo){    
//        
//        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
//          $.ajax({
//
//              type: "POST",
//              url: baseUrl + "vehiculos/eliminar_pjax",
//
//              data: {
//                vehiculo: vehiculo,
//              }
//
//          })
//               .done(function (data) {
//                    $.pjax.reload({container:"#vehiculos-pjax",timeout: false});
//                    
//                   // $("#mygrid").load("#mygrid");
//                    //$( "#mensaje" ).html( data );
//          });
//    }   
    
    
</script>