<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\bootstrap\Modal;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\models\clientes;
use app\models\vehiculos;
//use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Parte1 */
/* @var $form yii\widgets\ActiveForm */
//setlocale(LC_TIME,"es_ES");
$estado_presupuesto = [0 => 'Pendiente', 1 => 'Aceptado']; 

////$datajson = Json::decode($datos_autoc);
//$data =  ArrayHelper::toArray($datos_autoc); 
//var_dump($repuestos_autoc);

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.css" integrity="sha512-9iWaz7iMchMkQOKA8K4Qpz6bpQRbhedFJB+MSdmJ5Nf4qIN1+5wOVnzg5BQs/mYH3sKtzY+DOgxiwMz8ZtMCsw==" crossorigin="anonymous" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.js" integrity="sha512-yo/DGaftoLvI3LRwd6sVDlo4zu1AhQg+ej3UruAEzwWuzmNZglbF3luwTif1l9wvHZmLRp8Wyiv8wloA9JsVyA==" crossorigin="anonymous"></script>
<div class="parte1-form">
   <div class="container"> 
        <!--<div class="row">-->
            <?php $form = ActiveForm::begin(); ?>
<!--            <form action="/hms/accommodations" method="GET"> -->
            <div class="row" id="buscador-autocomplete" style="display:none">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="input-group"><label style="margin-right: 15px;">Buscar Vehiculo/Cliente  </label>

                        <?= AutoComplete::widget([  
                            "id" => "auto",
                            'clientOptions' => [
                            'source' => $datos_autoc,
                            'minLength'=>'3', 
                            'autoFill'=>true,
                            'select' => new JsExpression("function( event, ui ) {
                                                    $('#vehiculo').val(ui.item.id);
                                                      var vehiculo = ui.item.id;
                                                      datoscabecera(vehiculo);

                                                    //#memberssearch-family_name_id is the id of hiddenInput.
                                                 }")],
                             'options' => ['style'=>'width: 400px;border-radius:5px;background-color:#f5f5f5;border-color:#ccc']
                                                 ]);
                ?> <hr>
                        <div class="input-group-btn">
                            
                        </div>
                      </div>
                    </div>
<!--                    <div class="col-md-6" align="right"> 
                        <div class="input-group">
                            <input type="text" value="Facturacion Marzo: 2585.86" style="width:300px;text-align: center"/>
                        </div>
                    </div>
                    -->
                    
                    
                    <!--
                <div class="col-xs-6 col-md-5">
                 <div class="input-group">
                   <input type="text" class="form-control" placeholder="Buscar" id="txtSearch"/>
                   <div class="input-group-btn">
                     <button class="btn btn-primary" type="buton ">
                       <span class="glyphicon glyphicon-search"></span>
                     </button>
                   </div>
                 </div>
               </div>  -->
                  
                  
              </div>
            </div>


            <!--</form>-->
            
        <!--</div>-->
       <div class="row">
           <div class="col-xs-6 col-sm-3 col-md-3" style="border-radius:25px;border:1px solid;padding:15px 15px 15px 15px;margin: 20px 80px 20px 20px;border-color: #286092">
                <?= Html::input('text', 'nombre', 'Cliente', ['class' => 'form-control','id'=> 'nombre']) ?> 
                <!--Html::input('text', 'direccion', 'Direccion', ['class' => 'form-control','id'=> 'direccion'])-->  
                <?= Html::input('text', 'poblacion', 'Poblacion', ['class' => 'form-control','id'=> 'poblacion']) ?> 
                <?= Html::input('text', 'cif_dni', 'CIF/DNI', ['class' => 'form-control','id'=> 'cif_dni']) ?> 
           </div>
           <div class="col-xs-6 col-sm-3 col-md-3" style="border-radius:25px;border:1px solid;padding:15px 15px 15px 15px;margin: 20px 80px 20px 20px;border-color: #286092">
                <?= Html::input('text', 'marca', 'Marca/Modelo', ['class' => 'form-control','id'=> 'marca']) ?> 
                <?= Html::input('text', 'matricula', 'Matricula', ['class' => 'form-control','id'=> 'matricula']) ?> 
                <?= Html::input('text', 'bastidor', 'Bastidor', ['class' => 'form-control','id'=> 'bastidor']) ?>  
              
           </div>
            <div class="col-xs-6 col-sm-3 col-md-3" style="border-radius:25px;border:1px solid;padding:-15px 20px 20px 15px;margin: 20px 10px 10px 10px;border-color: #286092;">
                 <!--<span style="font-weight:bold;font-size: 12px;">Fecha</span>-->
                <!--$form->field($model, 'fecha_presupuesto')->textInput(['style'=>'margin-top:-20px;','placeholder'=>'Fecha','id'=>'fecha_presu','value'=>date('d-m-Y')])->label('')--> 
                  <?= $form->field($model, 'fecha_presupuesto')->widget(DatePicker::className(), [
                'language' => 'es',
                
                //'size' => 'ms',
               // 'template' => '{input}',
                
                'inline' => false,
                'options' => ['class' => 'campo-cabe'],
                'clientOptions' => [
//                    'startView' => 1,
//                    'minView' => 0,
//                    'maxView' => 1,
                    'autoclose' => true,
                     'format' => 'dd-mm-yyyy',
                    'clearBtn' => true,
                    
                    //'format' => 'yyyy-m-d',
                    //'linkFormat' => 'HH:ii P', // if inline = true
                    // 'format' => 'HH:ii P', // if inline = false
                    'todayBtn' => true
                ]
            ]);?>
                 
             
                 
                 
                 
               	<!--<span style="font-weight:bold;font-size: 12px;margin-top:-20px;">Presupuesto  </span>-->
               
                <div class="col-xs-12" style="margin-top:-10px">
                    <div class="col-xs-6">
                        <?= $form->field($model, 'id')->textInput(['style'=>'text-align:center;','placeholder'=>'','id'=>'parte'])->label('Parte') ?>
                    </div> 
                      <div class="col-xs-6">    
                    <?= $form->field($model, 'nfactura')->textInput(['style'=>'text-align:center;','placeholder'=>'','id'=>'factura'])->label('Factura') ?>
                    </div>
                </div>    
              
         </div>
         <div class="col-xs-6 col-sm-3 col-md-3" style="display:none">
              
              <?= $form->field($model, 'nparte')->textInput(['style'=>'margin-top:-75px;display:none','placeholder'=>'Parte Nº','id'=>'parte_prov'])->label('') ?>
            
              
         </div>
     </div>  
        <div class="row" >
            <div class="col-xs-6 col-sm-3 col-md-3">
              <?= $form->field($model, 'entrada')->widget(DatePicker::className(), [
                'language' => 'es',
                //'size' => 'ms',
               // 'template' => '{input}',
                
                'inline' => false,
                'options' => ['class' => 'campo-cabe'],
                'clientOptions' => [
//                    'startView' => 1,
//                    'minView' => 0,
//                    'maxView' => 1,
                    'autoclose' => true,
                     'format' => 'dd-mm-yyyy',
                    'clearBtn' => true,
                    //'format' => 'yyyy-m-d',
                    //'linkFormat' => 'HH:ii P', // if inline = true
                    // 'format' => 'HH:ii P', // if inline = false
                    'todayBtn' => true
                ]
            ]);?>
               
           </div>
           <div class="col-xs-6 col-sm-3 col-md-3">
              <?= $form->field($model, 'salida')->widget(DatePicker::className(), [
                'language' => 'es',
                //'size' => 'ms',
               // 'template' => '{input}',
                
                'inline' => false,
                'options' => ['class' => 'campo-cabe'],
                'clientOptions' => [
//                    'startView' => 1,
//                    'minView' => 0,
//                    'maxView' => 1,
                    'autoclose' => true,
                     'format' => 'dd-mm-yyyy',
                    'clearBtn' => true,
                    //'format' => 'yyyy-m-d',
                    //'linkFormat' => 'HH:ii P', // if inline = true
                    // 'format' => 'HH:ii P', // if inline = false
                    'todayBtn' => true
                ]
            ]);?>
             
           </div>
           <div class="col-xs-6 col-sm-2 col-md-2">
              
                 <?= $form->field($model, 'estado')->dropDownList($estado_presupuesto, ['class' => 'form-control campo-cabe','placeholder'=>'Estado'])->label('Estado') ?>
           </div>
           <div class="col-xs-6 col-sm-2 col-md-2">
             <!--$form->field($model, 'kms')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999.999','class' => 'form-control'])->label('Kms') ?>-->
              <?= $form->field($model, 'kms')->textInput(['class' => 'form-control campo-cabe','placeholder'=>'Kms'])->label('Kms') ?>
           </div>
            <div class="col-xs-6 col-sm-1 col-md-1">
                <?= $form->field($model, 'dto')->textInput(['class' => 'form-control campo-cabe','placeholder'=>'Dto','id'=>'dto','style'=>'text-align:center'])->label('% Dto') ?>
          
           </div>
            <div class="col-xs-6 col-sm-1 col-md-1">
                <?= $form->field($model, 'iva')->textInput(['value'=>'21','class' => 'form-control campo-cabe','placeholder'=>'IVA','id'=>'iva','style'=>'text-align:center'])->label('% Iva') ?>
             
           </div>
        </div>   
                <?= (isset($_REQUEST['vehiculo'])) ? $form->field($model, 'vehiculo')->textInput(['value'=>$_REQUEST['vehiculo'],'id'=>'vehiculo','style'=>'display:none'])->label(''):
                    $form->field($model, 'vehiculo')->textInput(['style'=>'display:none','id'=>'vehiculo'])->label('') ?>
         <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'descripcion')->textarea(['rows' => 2,'style'=>'margin-top:-45px','placeholder'=>'Descripción de la reparación','class' => 'form-control campo-cabe'])->label('') ?>
            </div>
        </div>
       
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'envioForm','style'=>'width:120px;']) ?>
        <?= Html::Button('Imprimir', ['class' => 'btn btn-primary','id'=>'imprimir_parte','style'=>'width:120px;']) ?>
        <?= Html::Button('Facturar', ['class' => 'btn btn-primary','id'=>'facturar_parte','style'=>'width:120px;']) ?>
        <?= Html::Button('Enviar Correo', ['class' => 'btn btn-primary','id'=>'correo_facturacion','style'=>'width:120px;']) ?>
        <?= Html::Button('Historial Facturación', ['class' => 'btn btn-primary','id'=>'historial_facturacion','style'=>'width:150px;']) ?>
        
    </div>
    

    <?php ActiveForm::end(); ?>

</div>


  <!--ventana modal para actualizar vehiculos-->
<div id="modal_actualizar_modulo">
    <?php Modal::begin([
        'id'=>'modal_act_detalle_parte',
        'size'=>'modal-lg',
        'header' => '<h2>Actualizacion de detalle reparacion </h2>',
            //'toggleButton' => ['label' => 'click me'],
    ]);

        echo "<div id='modalContent'></div>";

    Modal::end();?>
</div>
    <!--ventana modal para crear repuestos nuevos-->
<div id="modal_crear_repuestos">
    <?php Modal::begin([
        'id'=>'modal_repuestos',
        'size'=>'modal-lg',
        'header' => '<h2>Creación de repuestos </h2>',
            //'toggleButton' => ['label' => 'click me'],
    ]);

        echo "<div id='modalContent'></div>";

    Modal::end();?>
</div>
    
<div id="modal_facturacion">
    <?php Modal::begin([
        'id'=>'modal_facturas',
        'size'=>'modal-sx',
        'header' => '<h2>Historial de Facturación</h2>',
            //'toggleButton' => ['label' => 'click me'],
    ]);?>

    <div id="modalContent" style="height: 600px;">
            <div class="col-md-8">
                <label>Año de Facturación</label>
                <select id="anyo_facturacion" class="form-control" style="width:200px;">
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>        
                </select>
            </div>
            <div class="col-md-12" id="tbl-facturas">
            </div>
            
        </div>
<?php
    Modal::end();?>
</div>    
  
    <!--ventana modal para crear impresos pdf-->
  <div id="modal_crear_impresos" style="background-color:#f5f5f5;text-align:center">
    <?php Modal::begin([
      'id'=>'modal_impresos',
      'size'=>'modal-sm',
      'header' => '<h2>Impresos </h2>',
  		//'toggleButton' => ['label' => 'click me'],
	]);?>
    <div class="row" style="padding:20px">
      <table align="center">
        <tr>
          <td>
      		<?=Html::a( Html::img(url::to('@web/img/presupuesto.png'),['alt'=>'Presupuesto','style'=>'border-radius:40px;width:130px;padding-left:10px;margin-left:15px;','title'=>'Presupuesto']),['site/pdf_parte','parte'=>$model->id,'tipo'=>'P'],['target'=>'_blank','title'=>'Presupuesto']);?>      
          </td>
        </tr> 
        <tr>
           <td align="center"> <span style="font-weight:bold">Presupuesto</span></td>
        </tr>  
        <tr>  
          <td>
     		<!--<span style="font-weight:bold">Presupuesto</span>-->
      		<?=Html::a( Html::img(url::to('@web/img/parte_reparacion.png'),['alt'=>'Parte Reparacion','style'=>'border-radius:80px;width:160px;padding-left:10px;margin-top:25px;']),['site/pdf_parte','parte'=>$model->id,'tipo'=>'R'],['target'=>'_blank','title'=>'Parte de Reparación']);?> 
          </td>
        </tr> 
        <tr>
           <td align="center"><span style="font-weight:bold">Parte Reparación</span></td>
        </tr>  
        <tr>  
          <td>
           <!--<span style="font-weight:bold">Reparación</span>-->
          <?=Html::a( Html::img(url::to('@web/img/factura.png'),['alt'=>'Factura','style'=>'border-radius:180px;width:140px;padding-left:10px;margin-top:20px;']),['site/pdf_parte','parte'=>$model->id,'tipo'=>'F'],['target'=>'_blank','title'=>'Factura']);?>
     		 <!-- <span style="font-weight:bold">Factura</span> -->
          </td>
        </tr> 
        <tr>
           <td align="center"> <span style="font-weight:bold">Factura</span></td>
        </tr>
      </table>
    </div>                          
    <?php Modal::end();?>
  </div>

<hr>
 
<h4>Detalle de la Reparación</h4></div>
   

<div class="form-inline row" id="form_detalle" style="margin-botton:10px;">
       <div class="col-xs-11">
    <!--    <p id='msjs_form' style='text-align: center;background-color: #F5F5F5;width: 500px;margin-left: 300px;font-weight: bold;border-radius: 40px;'></p>-->
            <?= Html::input('text', 'codigo_id', Yii::$app->request->post('codigo_id'), ['class' => 'form-control','id'=> 'codigo_id','style'=>'display:none']) ?>
            <?= Html::input('text', 'idparte', Yii::$app->request->post('idparte'), ['class' => 'form-control','id'=> 'idparte','style'=>'display:none']) ?>
         <!--Autocomplete de repuestos-->  
            <?= AutoComplete::widget([  
           "id" => "repuesto",
           'clientOptions' => [
           'source' => $repuestos_autoc,
           'minLength'=>'3', 
           'autoFill'=>true,
           'select' => new JsExpression("function( event, ui ) {
                                   $('#repuesto_inser').val(ui.item.referencia);
                                   $('#descripcion').val(ui.item.descripcion);


                                   //#memberssearch-family_name_id is the id of hiddenInput.
                                }")],
            'options' => ['class'=>'form-control','style'=>'background-color:#f5f5f5;border-color:#ccc','placeholder'=>'Buscar Repuesto']
                                ]);
        ?>
         <button type="button" class="btn btn-primary" style="margin-left:-20px;" value="<?= Url::to('/web/index.php/repuestos/create_modal')?>" id="anadir_repuesto">
             
                <i class="fas fa-plus"></i>
         </button>
           
           
           
            <?= Html::input('text', 'repuesto', Yii::$app->request->post('repuesto'), ['class' => 'form-control','placeholder'=>'Repuesto','style'=>'display:none','id'=> 'repuesto_inser']) ?>
            <?= Html::input('text', 'descripcion', Yii::$app->request->post('descripcion'), ['class' => 'form-control','placeholder'=>'Descripcion','id'=> 'descripcion']) ?>
            <?= Html::input('text', 'cantidad', Yii::$app->request->post('cantidad'), ['class' => 'form-control','placeholder'=>'Cantidad','id'=> 'cantidad']) ?>
            <?= Html::input('text', 'dto_int', Yii::$app->request->post('dto'), ['class' => 'form-control','placeholder'=>'Dto','id'=> 'dto_int']) ?>
            <?= Html::input('text', 'importe', Yii::$app->request->post('importe'), ['class' => 'form-control','placeholder'=>'importe','id'=> 'importe']) ?>
        </div>
        <div class="col-xs-1">
            <?= Html::Button('Añadir', ['class' => 'btn btn-primary', 'name' => 'nuevo_detalle','id' => 'nuevo_detalle','style'=>'width:100px;margin-left:-20px;']) ?>
         </div>
</div>






<div id="mygrid" style="margin-top: 15px;">
<?php Pjax::begin(['id'=>'detalle-pjax']);
    $base = 0;
    foreach ($dataProvider->models as $model) {
        $base += ($model->cantidad * $model->importe)-(($model->cantidad * $model->importe)*(intval($model->dto)/100));
    }
//    $subtotal_pie = $base *($_REQUEST['dto']/100);
//    $iva = $subtotal_pie *($_REQUEST['iva']/100);
//    $total_factura = $subtotal_pie + $iva;
?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'id' => 'myGrid_',
        // 'showFooter' => true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id_parte1'
            [
             'label'=>'Repuesto',
             'attribute'=>'codigo',
             'contentOptions' => ['style' => 'width:175px'],
            ],
            [
             'label'=>'Descripción',
             'attribute'=>'descripcion',
             'contentOptions' => ['style' => 'width:250px'],
            ],
            //'codigo',
            //'descripcion',
             [
             'label'=>'Un.',
             'attribute'=>'cantidad',
             'headerOptions' => ['style' => 'text-align:center'],
             'contentOptions' => ['style' => 'text-align:right;width:40px'],
            ],
            [
             'label'=>'Dto',
             'attribute'=>'dto',
             'format'=>'raw',
             //'headerOptions' => ['style'=>'text-align:center'],
             'contentOptions' => function ($model) {
                             return ['style' => 'text-align:center;color:' 
                                 .($model->dto > 0 ? '#337ab7' : '')];
                            },  
             'headerOptions' => ['style' => 'width:10px;text-align:center'],
             
             //'contentOptions' => [0=>['style'=>'color:red'],1=>['style'=>'color:blue']],   
             'value' => function($model) {
                            return $model->dto == '' ? '' : $model->dto;}             
            ],
          // 'dto',
            [
            'label' => 'Importe',
            'attribute' => 'importe', 
            'headerOptions' => ['style' => 'text-align:center'],
            'contentOptions' => ['style' => 'text-align:right;width:70px'],
            'format'=>['decimal',2],
            ],
           [
            'label' => 'Subtotal',
            'value' => function ($model){
                      return ($model->cantidad * $model->importe)-(($model->cantidad * $model->importe)*(intval($model->dto)/100));
                 },
            'contentOptions' => ['style' => 'text-align:right;width:100px'],
            'format'=>['decimal',2],
            'headerOptions' => ['style' => 'text-align:center;color:#337ab7;'],
               //'footer' => '<p>BASE:'.$base.'</p><p>DTO. %:</p><p>SUBTOTAL:</P><P>I.V.A. 21 %:</p><p>TOTAL:<p>',
            ],
             
            ['class' => 'yii\grid\ActionColumn',
                
            'contentOptions' => ['style' => 'width:100px;text-align:center'],
            'header'=>'Opciones',
            'headerOptions' => ['style' => 'text-align:center'],
            'template' => '{update}{delete}',
            'buttons' => [
  
                             'update' => function ($url,$model,$key) {
                                return  Html::button(
                                '<span class="glyphicon glyphicon-pencil" style="font-size:20px" id="ico_actualiza"></span>',['value' => Url::to('/web/index.php/parte2/updatemodal'.'?id='.$model->id),'codigo' =>$model->codigo,'descripcion' =>$model->descripcion,'cantidad' =>$model->cantidad,'dto' =>$model->dto,'importe' =>$model->importe,'id'=>'modalBtnUpdate','style'=>'border:none;background-color:transparent;text-decoration:none']); 
                               
                                       //         return Html::button(  																   
   										//'<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>',['value' =>'web/index.php/parte2/updatemodal'.'?id='.$model->id,'id'=>'modalBtnUpdate','style'=>'border:none;background-color:transparent;']);                                                    
//                                                    ['data'=>[
//                                                                'method' => 'get',
//                                                                'params'=>['id'=>$model->id],                                         
//                                                                ]
//                                                    ]
//                                                        )
                                },
                            'delete' => function ($url, $model, $key) {
                             
                                return  Html::a(
                                '<span class="glyphicon glyphicon-trash" id ="" style="padding-left:5px;font-size:20px"></span>',['parte2/eliminar_pjax'.'?id='.$model->id],                                                            
                                                         ['data'=>[
                                                                'method' => 'post',
                                                                'confirm' => 'Estas Seguro de eliminar?',
                                                                'params'=>['parte'=>$model->id_parte1],
                                                                'id'=>'btnEliminaDetalleParte',
                                                                ]
                                                        ]);
  
                            }
                        ]
            ],  

        ],
    ]); ?>
    
     
<?php Pjax::end(); ?>
    
  
 </div> 

     
     <div class="row">
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:50px" align="center">
            <span style="color:#2398D6;font-weight: bold;" >Base: </span>
            <span  id ="base" value ="<?=$base?>" style="text-align:center"><?=$base?></span><span>€</span>
           <!--Html::input('text', 'base',$base, ['class' => 'form-control','id'=>'base','style'=>'text-align:center']) ?>-->
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">Dto %: </span>
            <span id ="dto_pie" style="text-align:center"></span>
            <!--Html::input('text', 'dto_pie','', ['class' => 'form-control','id'=>'dto_pie','style'=>'text-align:center'])--> 
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">Subtotal: </span>
            <span id ="subtotal_pie" style="text-align:center"></span>
             <!--Html::input('text', 'subtotal','', ['class' => 'form-control','id'=>'subtotal_pie','style'=>'text-align:center']) ?>-->
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">I.V.A.: </span>
            <span id ="iva_pie" style="text-align:center"></span>
           <!--Html::input('text', 'iva_pie','', ['class' => 'form-control','id'=>'iva_pie','style'=>'text-align:center']) ?>-->
        </div>
        <div class="col-xs-2 col-sm-2" style="border:solid 1px grey;border-radius: 5px;margin-left:15px" align="center">
            <span style="color:#2398D6;font-weight: bold;">TOTAL: </span>
            <span id ="total_factura" style="text-align:center"></span>
            <!--Html::input('text', 'total_factura','', ['class' => 'form-control','id'=>'total_factura','style'=>'text-align:center']) ?>-->
        </div>
    </div>
   
</div>


<script>
$( document ).ready(function() {
//     console.log( $('#codigo_cliente').html());
  
  
    if ( $('#vehiculo').val() >0 ){ 
        var vehiculo = $('#vehiculo').val();
        datoscabecera(vehiculo);
    }else{
        $('#buscador-autocomplete').show();
    }
 
  
    if ( $('#parte').val() != '' ){  
        $('#parte_prov').val($('#parte').val());  
        $('#idparte').val($('#parte').val());  
    }
    
   
    
    //vamos calculando los totales al pie del gridview
    var base = parseFloat($('#base').text()).toFixed(2);
    var dto = $('#dto').val();
    var iva = $('#iva').val();           
    var tot_dto = parseFloat(base *(dto/100)).toFixed(2);
    var subtotal = parseFloat(base -  tot_dto).toFixed(2);
    var tot_iva = parseFloat((subtotal * iva)/100).toFixed(2);
    var total_factura = parseFloat(parseFloat(subtotal) + parseFloat(tot_iva)).toFixed(2);
    $('#base').text(base);
    $('#dto_pie').text(tot_dto+' €');
    $('#subtotal_pie').text(subtotal+' €');
    $('#iva_pie').text(tot_iva+' €');
    $('#total_factura').text(total_factura+' €');
    
    
    $(document).on('pjax:error', function(event, xhr) {
         $('#mensaje').html('Datos introducidos erroneos o linea duplicada');  
//        alert('Pjax failed!');
//        console.log(xhr.responseText);
//        event.preventDefault();
    });
        $('body').on('click', '#auto', function() {
           $('#vehiculo').val('');
        });
     
        
      $("#nuevo_detalle").click(function(){
          insertarLinea();
      });
      
           
         $(document).on("click", "#modalBtnUpdate", function () {
        
        //console.log($("#modalBtnUpdate").attr('descripcion'));
         $('#modal_act_detalle_parte').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
		 //  $("#cod_id").val($("#modalBtnUpdate").attr('value'));
          // $("#repuesto").val($("#modalBtnUpdate").attr('codigo'));
          // $("#descripcion").val($("#modalBtnUpdate").attr('descripcion'));
          // $("#cantidad").val($("#modalBtnUpdate").attr('cantidad'));
          // $("#dto_int").val($("#modalBtnUpdate").attr('dto_int'));
          // $("#importe").val($("#modalBtnUpdate").attr('importe'));
          
            
     });
     
    $(document).on("click", "#imprimir_parte", function () {
        
        $('#modal_impresos').modal('show')
           //.find('#modalContent')
           //.load($(this).attr('value'));
    });  
    
    $(document).on("click", "#anadir_repuesto", function () {
        
        $('#modal_repuestos').modal('show')
           .find('#modalContent')
           .load($(this).attr('value'));
    }); 
     $(document).on("click", "#historial_facturacion", function () {
        $('#modal_facturas').modal('show');
           //.find('#modalContent')
           //.load($(this).attr('value'));
    });  
    
    
           
    $(document).on("click", "#facturar_parte", function () {
        //Se requiere tener introducida la fecha de salida que será la de la factura
        if ( ! $('#parte1-salida').val()){
             alert("Imposible facturar sin Fecha de salida asignada");
         }else{
          facturar($('#parte1-salida').val());
        }
       
    });  
      
    $(document).on("change", "#anyo_facturacion", function () {
        facturacion($('#anyo_facturacion').val());
    });    
      
    $(document).on("click", "#correo_facturacion", function () {
        correo();
    });   
    
     $(document).on("change", ".campo-cabe", function () {
         $('#envioForm').click();
         $("#envioForm").fadeToggle('slow');  
         $('#envioForm').html('Guardado');
         $('#envioForm').css('color','yellow');
       
//         $("#logo").hide().fadeIn('8000000',function()
//            {
//               $('#envioForm').html('Guardado');
//            }
//            ).fadeOut('slow');
        });    
   

    //controlamos si es un parte nuevo o una actualizacion del existente
    
        if($('#envioForm').hasClass("btn btn-success") ){
            //Si es un parte nuevo deshabilitamos la introduccion de repuestos hasta que no se grabe el parte de cabecera
            $('#form_detalle div').children().prop('disabled', true);
            //Introducimos la fecha del dia en la fecha de presupuesto
            //console.log(FechaActual());
//            $('#fecha_presu').text(FechaActual());
        }  
        
          if($('#fecha_presu').val('')){
            $('#fecha_presu').text(FechaActual());
          }
        //si el parte está ya facturado deshabilitamos los botones de actualizar y añadir repuestos
        
           if($('#envioForm').hasClass("btn btn-primary") && $('#factura').val() > 0 ){
             //console.log($('#factura').val());
            //Si es un parte nuevo deshabilitamos la introduccion de repuestos hasta que no se grabe el parte de cabecera
            $('#envioForm').prop('disabled', true);
            $('#facturar_parte').prop('disabled', true);
            $('#modalBtnUpdate').prop('disabled', true);
            $('#myGrid_ a').prop('disabled', true);
            $('#form_detalle div').children().prop('disabled', true);
            $('#mygrid div').children().prop('disabled', true);
            
            
            //Introducimos la fecha del dia en la fecha de presupuesto
            //console.log(FechaActual());
//            $('#fecha_presu').text(FechaActual());
        } 
}); 


    function insertarLinea(){    
        
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

            type: "POST",
            url: baseUrl + "parte2/detalle_parte",

            data: {
              idparte: $("#parte").val(),
              codigo_detalle:$("#cod_id").val(),
              repuesto: $("#repuesto_inser").val(),
              descripcion: $("#descripcion").val(),
              cantidad: $("#cantidad").val(),
              dto: $("#dto_int").val(),
              importe: $("#importe").val(),

            }

          })
        .done(function (data) {
            $.pjax.reload({container:"#detalle-pjax",timeout: false});
            $.pjax.reload({container:"#mygrid",timeout: false});
        });
    }
    function datoscabecera(vehiculo){    
        var vehiculo = vehiculo;
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

              type: "get",
              url: baseUrl + "vehiculos/datos_cabecera",

              data: {
                vehiculo: vehiculo,

              }
          })
               .done(function (data) {
                var obj = JSON.parse(data);
                $.each( obj, function( key, value ) {  
                    $('#nombre').val(value['nombre']+" "+ value['apellidos']);  
                    $('#poblacion').val(value['localidad']);
                    $('#cif_dni').val(value['cif']);
                    $('#marca').val(value['marca']);
                    $('#matricula').val(value['matricula']);
                    $('#bastidor').val(value['bastidor']);
                      
                });
//                
//                 document.write(obj.nombre);
                   
          });
    }
    function facturar(fecha_salida){    
        var fechaSalida = fecha_salida;
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

            type: "POST",
            url: baseUrl + "facturas/facturar",

            data: {
                //contenido del input menos 2 posiciones
                fechasalida: fechaSalida,
                idparte: $("#idparte").val(),
                basefactura: $('#base').text(),
                tipoiva: $('#iva').val(),
                subtotal: $('#subtotal_pie').text().substring(0, $('#subtotal_pie').text().length - 2),
                ivafactura: $('#iva_pie').text().substring(0, $('#iva_pie').text().length - 2),
                totalfactura: $('#total_factura').text().substring(0, $('#total_factura').text().length - 2),
            }

          })
        .done(function (data) {
           // console.log(data);
           
        });
    }
        function facturacion(anyo){    
        
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
        var anyo_facturacion = anyo;
          $.ajax({

            type: "POST",
            url: baseUrl + "facturas/facturacion",

            data: {
                //contenido del input menos 2 posiciones
                anyo: anyo_facturacion,
            }

          })
        .done(function (data) {
            var datos = JSON.parse(data);
//            console.log(datos[0].mes);
           eliminar_tabla_facturacion(); 
          
//                    $.each(datos, function(i,item) {  
                        //console.log(item.mes);
                        let elemento = '<table id="tbl_tr" class="table table-sm" style="width:400px;margin-top:20px;margin-left:50px;font-size: 16px;"><th style="text-align: center;background-color:#D2E0E6;border:1px solid grey;">Mes</th><th style="text-align: center;background-color:#D2E0E6;border:1px solid grey;">Total</th>'; 
                         for(let i = 0; i < datos.length; i++) {
//                console.log(data.items[i]);  // (o el campo que necesites)
                        
                        let mes =  datos[i].mes;   
                        let total = datos[i].total;
                      
                       elemento += '<tr><td style="text-align: center;border:1px solid grey;font-size:14px">'+mes+'</td>\n\
                                        <td style="text-align: center;border:1px solid grey;font-size:14px">'+total+' €'+'</td></tr>'; 
                    }
                      elemento += '</table>';
                     $('#tbl-facturas').append(elemento);
//        });
    });
    }
    
        function eliminar_tabla_facturacion(){
            $('#tbl-facturas table').remove();
        }
        function correo(){    
        
        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

            type: "POST",
            url: baseUrl + "facturas/enviarfacturas",

            data: {
                //contenido del input menos 2 posiciones
//                idparte: $("#idparte").val(),
//                basefactura: $('#base').text(),
//                tipoiva: $('#iva').val(),
//                subtotal: $('#subtotal_pie').text().substring(0, $('#subtotal_pie').text().length - 2),
//                ivafactura: $('#iva_pie').text().substring(0, $('#iva_pie').text().length - 2),
//                totalfactura: $('#total_factura').text().substring(0, $('#total_factura').text().length - 2),
            }

          })
        .done(function (data) {
          
        });
    }
    
    function FechaActual() {
       var tdate = new Date();
       var dd = tdate.getDate(); //yields day
       var MM = tdate.getMonth(); //yields month
       var yyyy = tdate.getFullYear(); //yields year
       var currentDate= dd + "-" +( MM+1) + "-" + yyyy;

       return currentDate;
    }
</script>
