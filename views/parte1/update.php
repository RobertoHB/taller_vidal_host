<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\Parte2;
/* @var $this yii\web\View */
/* @var $model app\models\Parte1 */

$this->title = 'Parte Reparación: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Parte', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
$this->params['breadcrumbs'][] = ['label' =>'Facturación Mensual: '.$model->getFacturacion()];
$this->params['breadcrumbs'][] = ['label' =>'Ultima Factura: '.$model->getUltimaFactura()];
if(isset($datos_autocomplete)){
    $datos_complete = $datos_autocomplete;
}else{
    $datos_complete = "";
}

if(isset($repuestos_autocomplete)){
    $repuestos_complete = $repuestos_autocomplete;
}else{
    $repuestos_complete = "";
}

$mensaje = "";
$detalle = new parte2();                        
$consulta = $detalle->find()
->where(['id_parte1'=>$model->id]);

$dataProvider = new ActiveDataProvider([
    'query' => $consulta,
    'pagination' => false,
]);

?>
<div class="parte1-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
        'datos_autoc' => $datos_complete,
        'repuestos_autoc' => $repuestos_complete,
        'mensaje' => $mensaje
    ]) ?>

</div>
