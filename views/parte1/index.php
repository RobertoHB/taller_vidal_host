<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Parte1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parte1-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevo Parte', ['create'], ['class' => 'btn btn-success','style'=>'background-color: #2395D1']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'vehiculo',
//            [
//              'label' => 'Nombre',
//             // 'attribute' => 'filtro_nombre',
//              'value' => 'cliente0.nombre',
//              //'filter' => ArrayHelper::map(Clientes::find()->all(), 'nombre','nombre'),
//              'headerOptions' => ['style' => 'width:120px'],
//              'enableSorting' => true,
//            ],
//            [
//              'label' => 'Apellidos',
//              //'attribute' => 'filtro_apellidos',
//              'value' => 'cliente.apellidos',
//              //'filter' => ArrayHelper::map(Clientes::find()->all(), 'nombre','nombre'),
//              'headerOptions' => ['style' => 'width:120px'],
//              'enableSorting' => true,
//            ],
            [
              'label' => 'Matricula',
              'attribute' => 'filtro_matricula',
              'value' => 'vehiculo0.matricula',
              //'filter' => $itemsApellidos,
              'enableSorting' => true,
              'headerOptions' => ['style' => 'width:150px;'],
            ],
           
            'entrada',
            [
             'label'=>'Salida',
             'attribute'=>'salida',
             'format'=>'raw',
             'value' => function($model) {
                       return $model->salida == '' ? '' : $model->salida;}  
            ],    
                    [
             'label'=>'Parte',
             'attribute'=>'nparte',
             'format'=>'raw',
             'value' => function($model) {
                       return $model->nparte == '' ? '' : $model->nparte;}  
            ],    
            [
             'label'=>'Factura',
             'attribute'=>'nfactura',
             'format'=>'raw',
             'value' => function($model) {
                       return $model->nfactura == '' ? '' : $model->nfactura;}  
            ],    
            [
             'label'=>'Estado',
             'attribute'=>'estado',
             'format'=>'raw',
             'value' => function($model) {
                       return $model->estado == '0' ? 'Pendiente' : 'Aceptado';}  
            ],    
            //'salida',
            //'nparte',
           // 'nfactura',
            'descripcion:ntext',
            //'estado',
            //'kms',
            //'dto',
            //'iva',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'contentOptions'=>[ 'style'=>'width: 100px;font-size:20px'],
                ],
                    
        ],
    ]); ?>


</div>
