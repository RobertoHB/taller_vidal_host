<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Parte1Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parte1-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vehiculo') ?>

    <?= $form->field($model, 'entrada') ?>

    <?= $form->field($model, 'salida') ?>

    <?= $form->field($model, 'nparte') ?>

    <?php // echo $form->field($model, 'nfactura') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'kms') ?>

    <?php // echo $form->field($model, 'dto') ?>

    <?php // echo $form->field($model, 'iva') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
