<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Facturas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="facturas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parte')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'factura')->textInput() ?>

    <?= $form->field($model, 'fecha')->widget(DatePicker::className(), [
        'language' => 'es',

        'inline' => false,
        'clientOptions' => [
            'autoclose' => true,
             'format' => 'dd-mm-yyyy',
            'clearBtn' => true,
            'todayBtn' => true
        ]
    ]);?>
    

    <?= $form->field($model, 'tipoiva')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'iva')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'subtotal')->textInput(['disabled' => true]) ?>

    <?= $form->field($model, 'total')->textInput(['disabled' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Anular',['facturas/delete','factura'=>$model->factura],['class' => 'btn btn-primary','data-method'=>'post','id'=>'anular_factura','style'=>'width:120px;']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
