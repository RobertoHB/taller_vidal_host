<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\vehiculos;
use app\models\Parte1;
use app\models\Facturas;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FacturasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facturas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facturas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nueva Factura', ['create'], ['class' => 'btn btn-success','style'=>'background-color: #2395D1']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'parte',
            'factura',
            'fecha',
            'tipoiva',
            'iva',
            'subtotal',
            'total',
            'comentarios',

           ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'contentOptions'=>[ 'style'=>'width: 100px;font-size:20px'],
            ],
        ],
    ]); ?>


</div>
