<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Clientes;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VehiculosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vehiculos';
$this->params['breadcrumbs'][] = $this->title;
$itemsApellidos =  ArrayHelper::map(Clientes::find()->select(['apellidos','id'])->asArray()->orderBy('apellidos')->all(),'apellidos', function ($model) {
                        return $model['apellidos'];
                        });
                        
if(isset($_REQUEST['mensaje'])){
    $mensaje = $_REQUEST['mensaje'];
}else{
    $mensaje ="";    
}                        
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
<div class="vehiculos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
         <!--Html::a('Nuevo Vehiculo', ['create'], ['class' => 'btn btn-success'])--> 
          <span id ="msg" style="margin-left:200px;background-color: #2191c0;color:white;font-size:20px;border:solid 1px #EEE;border-radius:15px;padding:5px;display:none"><?=$mensaje?></span>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'cliente',
             [
              'label' => 'Nombre',
              'attribute' => 'filtro_nombre',
              'value' => 'cliente0.nombre',
              'filter' => ArrayHelper::map(Clientes::find()->all(), 'nombre','nombre'),
              'headerOptions' => ['style' => 'width:120px'],
              'enableSorting' => true,
            ],
            [
              'label' => 'Apellidos',
              'attribute' => 'filtro_apellidos',
              'value' => 'cliente0.apellidos',
              'filter' => $itemsApellidos,
              'enableSorting' => true,
              'headerOptions' => ['style' => 'width:150px;'],
            ],
            [
            'label' => 'Matricula',
            'attribute' => 'matricula',
            'value' => 'matricula',
            'headerOptions' => ['style' => 'width:120px;'],
            ],
            'marca',
            [
            'label' => 'Color',
            'attribute' => 'color',
            'value' => 'color',
            'headerOptions' => ['style' => 'width:100px;'],
            ],
            'bastidor',
            
             [
            'label' => 'Combustible',
            'attribute' => 'combustible',
            'value' => 'combustible',
            'headerOptions' => ['style' => 'width:120px;'],
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                 'contentOptions'=>[ 'style'=>'width: 100px;font-size:20px'],
                ],
              [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{partes}',
              'contentOptions'=>['style'=>'width: 40px;font-size:20px'],
              'buttons' => [
              'partes' => function ($url, $model) {
                                  return Html::a('<span class="glyphicon glyphicon-wrench"></span>', $url, [
                                      'title' => Yii::t('app', 'Reparaciones'),
                                  ]);

                              }, 

                             ],

              'urlCreator' => function ($action, $model, $key, $index) {
                   return Url::to(['parte1/create', 'vehiculo' => $model->id]);
              }        

            ],
        ],
    ]); ?>


</div>


<script>
$( document ).ready(function() {
    
if($('#msg').html()!= ''){
     $("#msg").show();
   for(let i=0;i<5;i++){
            $("#msg").fadeToggle(1700);    
        }   
    $("#msg").fadeOut(); 
}     

    
});
</script>