<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vehiculos */
/* @var $form yii\widgets\ActiveForm */
$tiposcombustible = ['Gasolina' => 'Gasolina', 'Diesel' => 'Diesel','Gas Natural' => 'Gas Natural','Bioetanol' => 'Bioetanol','Bioediesel' => 'Biodiesel','Biogás' => 'Biogás','Eléctrico' => 'Eléctrico','Hidrógeno' => 'Hidrógeno','Solar' => 'Solar',]; 
?>

<div class="vehiculos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cliente')->textInput() ?>

    <?= $form->field($model, 'matricula')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'marca')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bastidor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'combustible')->dropDownList($tiposcombustible); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
