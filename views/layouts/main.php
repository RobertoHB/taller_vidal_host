<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <!--<title><?= Html::encode($this->title) ?></title>-->
    <?php $this->head() ?>
    <style>
                
        .links{
            background-color: #f5f5f5;
            /*background-color:#101010;*/
            color:black;
            font-weight:bold;
        }
        label:hover{
            background-color: #1f6377;
            color:black;
        }
     
      
        .my-navbar {
            background-color: #f5f5f5;
         /*background-color:#101010;*/
            border-top: 1px solid #ddd;
            margin-top:10px;
            margin-left:150px;
            border-radius:50px;
            width:80%;
            display:flex;
            justify-content: center;
            align-items: flex-end;
        }
       .menu{
          width: 775px !important;
       }
      button.navbar-toggle{
        background-color:black;
   		width:25x;
        margin-top:15px;
        margin-left:-10px;
        
      }
   
/*     //@media only screen and (max-width: 1440px) {
       // margin-left:-100px;
       // width:95%;
     // }*/
      
    @media only screen and (max-width: 375px) {
  		.my-navbar  {
          margin-left: -20px;
          width:400px;
          margin-top:-10px;
          padding-bottom:-20px;
          /*background-color:transparent;*/
  		}
      
     .navbar-brand > img{
           display:none;
      }
    }
    

   
 
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img(url::to('@web/img/logo.png'), ['width'=>'18%','alt'=>Yii::$app->name,'id'=>'logo']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'my-navbar navbar-fixed-top'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right menu','style'=>'font-size:17px;margin-top: -40px;'],
        'items' => [
            ['label' => 'Inicio', 'url' => ['/site/index'],'linkOptions' => ['class' => 'links']],
           // ['label' => 'About', 'url' => ['/site/about'],'linkOptions' => ['class' => 'links']],
            //['label' => 'Contact', 'url' => ['/site/contact'],'linkOptions' => ['class' => 'links']],
            ['label' => 'Clientes', 'url' => ['/clientes'],'linkOptions' => ['class' => 'links']],
            ['label' => 'Vehiculos', 'url' => ['/vehiculos'],'linkOptions' => ['class' => 'links']],
            ['label' => 'Repuestos', 'url' => ['/repuestos'],'linkOptions' => ['class' => 'links']],
            ['label' => 'Partes', 'url' => ['/parte1'],'linkOptions' => ['class' => 'links']],
            ['label' => 'Facturas', 'url' => ['/facturas'],'linkOptions' => ['class' => 'links']],
             ['label' => 'Informes', 'url' => ['/site/informes'],'linkOptions' => ['class' => 'links']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login'],'linkOptions' => ['class' => 'links']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container" style="margin-top:15px;">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; WerobSolutions <?= date('Y') ?></p>

        <!--<p class="pull-right"><?= Yii::powered() ?></p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
