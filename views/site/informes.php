<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use yii\data\ActiveDataProvider;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\swiftmailer;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use dosamigos\datepicker\DatePicker;
//use ArrayHelper;
use app\models\Repuestos;

$this->title = 'Generador de Informes';
$this->params['breadcrumbs'][] = $this->title;
$estado = [0 => 'Pendiente', 1 => 'Aceptado']; 
$conceptos = [0 => 'Cambio aceite', 1 => 'Correa Distribucion']; 


   $conceptos = ArrayHelper::map(Repuestos::find()->all(), 'referencia','descripcion');
        
//        $itemsAlumnos = ArrayHelper::map(Alumnos::find()->asArray()->select('id,apellidos,nombre')->orderBy('apellidos')->all(),'id','apellidos','nombre');
     
//        $itemsAlumnos =  ArrayHelper::map(Alumnos::find()->select(['id','apellidos','nombre'])->asArray()->orderBy('apellidos')->all(),'id', function ($model) {
//                        return $model['apellidos'] .' - '. $model['nombre'];
//                        });


?>

<style>
    
   .contiene_check{
       padding-left:5px;
       text-align: left;
      
    }
    .item{
        font-size: 16px;
        margin-top: 5px;
        width:290px;
        
    }
    .tableFixHead          { overflow-y: auto; height:700px; }
    .tableFixHead thead th { position: sticky; top: 0; }

    /* Just common table stuff. Really. */
    table  { border-collapse: collapse; width: 100%;}
    th, td { padding: 2px 4px; }
    th     { background:#eee; }
    
    select {
    font-family: 'Lato', 'Font Awesome 5 Free';
    font-weight: 900;
    }
    .listado{
        margin-left:50px;
        width:100%;
    }
    #listado_pjax{
        margin-top:25px;
    }
    .opciones{
      border:1px solid blanchedalmond;
      border-radius: 20px;
      width:310px;
      margin-top:20px;
    }
    
</style>
    
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

<div class="alumnos-index">
    <div class="container-fluid">
        <!--<div class = "row"><h1><?= Html::encode($this->title) ?></h1>-->
            <div class="col col-sm-4 opciones">
                <div class="form-group">
                    
                     <?php $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'method' => 'post',
                        'action' => ['site/exportar_excel']                         
                     ]); ?>

                        <?= $form->field($model, 'fechaInicio')->widget(DatePicker::className(), [
                            'language' => 'es',
                            //'size' => 'ms',
                           // 'template' => '{input}',

                            'inline' => false,
                            'clientOptions' => [
            //                    'startView' => 1,
            //                    'minView' => 0,
            //                    'maxView' => 1,
                                'autoclose' => true,
                                 'format' => 'dd-mm-yyyy',
                                'clearBtn' => true,
                                //'format' => 'yyyy-m-d',
                                //'linkFormat' => 'HH:ii P', // if inline = true
                                // 'format' => 'HH:ii P', // if inline = false
                                'todayBtn' => true
                            ]
                        ]);?>
                    
                    <?= $form->field($model, 'fechaFin')->widget(DatePicker::className(), [
                            'language' => 'es',
                            //'size' => 'ms',
                           // 'template' => '{input}',

                            'inline' => false,
                            'clientOptions' => [
            //                    'startView' => 1,
            //                    'minView' => 0,
            //                    'maxView' => 1,
                                'autoclose' => true,
                                 'format' => 'dd-mm-yyyy',
                                'clearBtn' => true,
                                //'format' => 'yyyy-m-d',
                                //'linkFormat' => 'HH:ii P', // if inline = true
                                // 'format' => 'HH:ii P', // if inline = false
                                'todayBtn' => true
                            ]
                        ]);?>
                    
                    
                    <?= $form->field($model, 'estado')->dropDownList($estado,['style'=>'margin-top:0px;','placeholder'=>'','id'=>'estado'])->label('estado')?>
                    <?= $form->field($model, 'concepto')->dropDownList($conceptos,['style'=>'margin-top:0px;','prompt' => '','id'=>'concepto'])->label('Concepto') ?>   
                      
                  
                    <div class="" style="margin-top:20px;margin-bottom: 20px;">                  
                      <?= Html::submitButton('Filtrar', ['class' => 'btn btn-block btn-lg', 'id'=>'filtrar','style'=>'width:275px;']) ?>
                    </div> 
        </div>

                </div> 
              
              
            
    
      <?php ActiveForm::end(); ?>
 
           <div class="col col-sm-8">
               <div class="listado tableFixHead" id="listado_pjax">
    <?php               
         $fullExportMenu = ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'target' => ExportMenu::TARGET_BLANK,
    'pjaxContainerId' => 'kv-pjax-container',
    'exportConfig' => [
        ExportMenu::FORMAT_PDF => [
            'pdfConfig' => [
                'methods' => [
                    'SetHeader' => ['Your Title Here'], 
                ],
            ],
        ],        
    ],    
             
             
    'exportContainer' => [
        'class' => 'btn-group mr-2'
    ],
    'dropdownOptions' => [
        'label' => 'Full',
        'class' => 'btn btn-outline-secondary',
        'itemsBefore' => [
            '<div class="dropdown-header">Export All Data</div>',
        ],
    ],
]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    //'showPageSummary' => true,
     //'layout' => "{footer}\n{items}\n<div class='d-flex justify-content-center'>{pager}</div>",
    'showFooter'=>TRUE,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class="panel-title"><i class="fas fa-book"></i> Library</h3>',
    ],
    // set a label for default menu
    'export' => [
        'label' => 'Formato',
    ],
//    'exportContainer' => [
//        'class' => 'btn-group mr-2'
//    ],
    // your toolbar can include the additional full export menu
    'toolbar' => [
        '{export}',
        $fullExportMenu,
//        [
//            'content'=>
//                Html::button('<i class="fas fa-plus"></i>', [
//                    'class' => 'btn btn-success',
//                    'title' => Yii::t('kvgrid', 'Add Book'),
//                    'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");'
//                ]) . ' '.
//                Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
//                    'class' => 'btn btn-outline-secondary',
//                    'title'=>Yii::t('kvgrid', 'Reset Grid'),
//                    'data-pjax' => 0, 
//                ]), 
//            'options' => ['class' => 'btn-group']
//        ],
    ]
]);

              
    ?>   
              
                  
               
           
               </div> 
           </div>  
        </div>
  
</div>



<script>
    $( document ).ready(function() {
//         $(document).on("click", "#filtrar", function () {
////            filtrar_datos();
// $.pjax.reload({container:"#informes-pjax"}); 
//           $.pjax.reload({container:"#listado_pjax"});
//        });  
   
 

    });   
    function filtrar_datos(){    
       //console.log($("#fecha_1").attr('value'));

        var baseUrl = "<?= Url::toRoute(["/"]); ?>"; 
          $.ajax({

            type: "POST",
            url: baseUrl + "site/exportar_excel",

            data: {
                //contenido del input menos 2 posiciones
                fechaInicio: $("#fecha_1").attr('value'),
                fechaFin: $('#fecha_2').attr('value'),
                estado: $('#estado').attr('value'),
                concepto: $('#concepto').text(),
            }

          })
        .done(function (data) {
         
           $.pjax.reload({container:"#informes-pjax"}); 
           $.pjax.reload({container:"#listado_pjax"});
        });
    }
</script>