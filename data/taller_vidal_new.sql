-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-03-2021 a las 19:09:40
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `taller_vidal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `rs` varchar(100) DEFAULT NULL,
  `cif_nif` varchar(15) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `cp` int(5) DEFAULT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `provincia` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `fijo` int(9) DEFAULT NULL,
  `movil` int(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `apellidos`, `rs`, `cif_nif`, `direccion`, `cp`, `localidad`, `provincia`, `email`, `fijo`, `movil`) VALUES
(1, 'Roberto', 'Fernández Blanco', '', '20195088F', 'Calle Alta 60 5B', 39008, 'Santander', 'Cantabria', 'robherblanc@gmail.com', 2147483647, 667743229),
(2, 'Javier', 'Hernández Blanco', '', '13697958J', 'Andres del rio 7', 39008, 'Santander', 'Cantabria', 'javierhb@gmail.com', 942055687, NULL),
(3, 'Veronica', 'Cabello Rojo', '', '21254788A', 'Calle Justicia 11 1º zda', 39008, 'Santander', 'Cantabria', 'veronica.cabello.rojo@gmail.com', 942378987, 665457788);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id` int(11) NOT NULL,
  `parte` int(11) DEFAULT NULL,
  `factura` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `tipoiva` int(3) DEFAULT NULL,
  `iva` double(5,2) DEFAULT NULL,
  `subtotal` double(5,2) DEFAULT NULL,
  `total` double(5,2) DEFAULT NULL,
  `comentarios` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id`, `parte`, `factura`, `fecha`, `tipoiva`, `iva`, `subtotal`, `total`, `comentarios`) VALUES
(6, 1, 1, '2021-03-16', 21, 19.84, 94.50, 114.34, ''),
(7, 1, 2, '2021-03-18', 21, 19.84, 94.50, 114.34, ''),
(8, 2, 3, '2021-03-18', 21, 66.15, 315.00, 381.15, ''),
(9, 3, 4, '2021-03-18', 21, 30.45, 145.00, 175.45, ''),
(10, 4, 5, '2021-04-09', 21, 18.46, 87.90, 106.36, ''),
(11, 5, 6, '2021-03-25', 21, 5.25, 25.00, 30.25, ''),
(12, 6, 7, '2021-03-25', 21, 26.25, 125.00, 151.25, ''),
(13, 7, 13, '2021-03-31', 21, 54.60, 260.00, 314.60, ''),
(16, 8, 8, '2021-03-27', 21, 31.50, 150.00, 181.50, ''),
(18, 9, 9, '2021-03-29', 21, 10.92, 52.00, 62.92, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parte1`
--

CREATE TABLE `parte1` (
  `id` int(11) NOT NULL,
  `vehiculo` int(11) DEFAULT NULL,
  `fecha_presupuesto` date DEFAULT NULL,
  `entrada` date DEFAULT NULL,
  `salida` date DEFAULT NULL,
  `nparte` int(11) DEFAULT NULL,
  `nfactura` int(11) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `kms` int(7) DEFAULT NULL,
  `dto` int(2) DEFAULT NULL,
  `iva` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `parte1`
--

INSERT INTO `parte1` (`id`, `vehiculo`, `fecha_presupuesto`, `entrada`, `salida`, `nparte`, `nfactura`, `descripcion`, `estado`, `kms`, `dto`, `iva`) VALUES
(1, 60, '2021-02-01', '2021-02-04', '2021-03-16', 1, 1, 'Cambio de aceite y filtrro. Cambiar escobillas delanteras y traseras. Cambio de 4 neumaticos', 1, 560125, 10, 21),
(2, 65, '2021-02-01', '2021-02-04', '2021-02-05', 2, 3, 'Cambiar liquido de frenos y 4 neumaticos', 1, 185000, NULL, 21),
(3, 64, '2021-02-15', '2021-02-16', '2021-02-19', 3, 4, 'Cambiar correa de distribución, aceite y filtro del aceite', 1, 185654, NULL, 21),
(4, 63, '2021-04-05', '2021-04-08', '2021-04-09', 4, 5, 'Cambio de aceite y filtros', 1, 157452, NULL, 21),
(5, 63, '2021-03-19', '2021-03-19', '2021-03-25', 5, 6, 'Cambio liquido de frenos', 1, 75845, NULL, 21),
(6, 65, '2021-03-19', '2021-03-22', '2021-03-25', 6, 7, 'Cambio aceite', 1, 45854, NULL, 21),
(7, 60, '2021-03-26', '2021-03-29', '2021-03-31', 7, 13, 'Cambio 4 neumáticos', 1, 365254, NULL, 21),
(8, 65, '2021-03-26', '2021-03-26', '2021-03-27', 8, 8, 'cambio aceite', 1, 45878, NULL, 21),
(9, 65, '2021-03-28', '2021-03-28', '2021-03-29', 9, 9, 'Cambio liquido frenos', 0, 145878, NULL, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parte2`
--

CREATE TABLE `parte2` (
  `id` int(11) NOT NULL,
  `id_parte1` int(11) DEFAULT NULL,
  `codigo` varchar(10) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `cantidad` int(3) DEFAULT NULL,
  `dto` int(2) DEFAULT NULL,
  `importe` double(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `parte2`
--

INSERT INTO `parte2` (`id`, `id_parte1`, `codigo`, `descripcion`, `cantidad`, `dto`, `importe`) VALUES
(2, 2, '-', 'liquido frenos', 1, NULL, 50.00),
(3, 2, '-', 'neumaticos pireli', 4, NULL, 52.00),
(4, 2, '-', 'Equilibrado ruedas', 4, 5, 15.00),
(11, 1, 'S3', 'Filtro de aceite', 1, NULL, 50.00),
(12, 1, 'S2', 'Filtro de aire', 1, NULL, 25.00),
(13, 3, 'S5', 'Correa de distribución', 1, NULL, 60.00),
(14, 3, 'S1', 'Aceite 15/40', 5, NULL, 7.00),
(15, 3, 'S3', 'Filtro de aceite', 1, NULL, 25.00),
(16, 1, 'S1', 'Aceite 15/40', 5, NULL, 6.00),
(17, 3, 'S2', 'Filtro de aire', 1, NULL, 25.00),
(18, 4, 'S1', 'Aceite 15/40', 5, NULL, 5.58),
(19, 4, 'S3', 'Filtro de aceite', 1, NULL, 35.00),
(20, 4, 'S2', 'Filtro de aire', 1, NULL, 25.00),
(21, 5, 'S7', 'Liquido de Frenos', 1, NULL, 25.00),
(22, 6, 'S1', 'Aceite 15/40', 5, NULL, 25.00),
(23, 7, 'S8', 'Neumaticos Michelin 195/200/R18', 4, NULL, 65.00),
(24, 8, 'S1', 'Aceite 15/40', 10, NULL, 15.00),
(25, 9, 'S7', 'Liquido de Frenos', 1, NULL, 52.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestos`
--

CREATE TABLE `repuestos` (
  `id` int(11) NOT NULL,
  `referencia` varchar(25) DEFAULT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `proveedor` varchar(50) DEFAULT NULL,
  `marca` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `repuestos`
--

INSERT INTO `repuestos` (`id`, `referencia`, `descripcion`, `proveedor`, `marca`) VALUES
(1, 'S1', 'Aceite 15/40', '', ''),
(2, 'S2', 'Filtro de aire', '', ''),
(3, 'S3', 'Filtro de aceite', '', ''),
(4, 'S4', 'Correa de distribución', '', ''),
(5, 'S5', 'Correa del alternador', '', ''),
(6, 'S6', 'Radiador', '', ''),
(7, 'S7', 'Liquido de Frenos', '', ''),
(8, 'S8', 'Neumaticos Michelin 195/200/R18', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(11) NOT NULL,
  `cliente` int(11) DEFAULT NULL,
  `matricula` varchar(10) DEFAULT NULL,
  `marca` varchar(25) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `bastidor` varchar(17) DEFAULT NULL,
  `combustible` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `cliente`, `matricula`, `marca`, `color`, `bastidor`, `combustible`) VALUES
(60, 1, '1369GZHAB', 'DACIA DUSTER', 'BEIGE', '1254785785YYH45RE', 'Diesel'),
(61, 1, '1368GZH', 'DACIA DUSTER', 'BEIGE', '1254785785YYH45RE', 'Diesel'),
(62, 1, '1369GZH', 'DACIA DUSTER', 'BEIGE', '1254785785YYH45RE', 'Diesel'),
(63, 1, '1364GZH', 'DACIA DUSTER', 'BEIGE', '1254785785YYH45RE', 'Diesel'),
(64, 2, 'S1240AB', 'RENAULT SCENIC', 'AZUL', '12478RTE8845AAAA1', 'Gasolina'),
(65, 3, '1428GZHA', 'FIAT TIPO', 'ROJO', '1254A254AF44E233F', 'Gasolina'),
(66, 2, '1369GZHABC', 'DACIA DUSTER', 'BEIGE', '1254785785YYH45RE', 'DIESEL'),
(67, 2, '1369GZHH', 'DACIA DUSTER', 'BEIGE', '1254785785YYH45RE', 'DIESEL');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_facturasPartes` (`parte`);

--
-- Indices de la tabla `parte1`
--
ALTER TABLE `parte1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vehiculosParte` (`vehiculo`);

--
-- Indices de la tabla `parte2`
--
ALTER TABLE `parte2`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ParteVehiculoParteReparacion` (`id_parte1`);

--
-- Indices de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vehiculosClientes` (`cliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `parte1`
--
ALTER TABLE `parte1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `parte2`
--
ALTER TABLE `parte2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `fk_facturasPartes` FOREIGN KEY (`parte`) REFERENCES `parte1` (`id`);

--
-- Filtros para la tabla `parte1`
--
ALTER TABLE `parte1`
  ADD CONSTRAINT `fk_vehiculosParte` FOREIGN KEY (`vehiculo`) REFERENCES `vehiculos` (`id`);

--
-- Filtros para la tabla `parte2`
--
ALTER TABLE `parte2`
  ADD CONSTRAINT `fk_ParteVehiculoParteReparacion` FOREIGN KEY (`id_parte1`) REFERENCES `parte1` (`id`);

--
-- Filtros para la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `fk_vehiculosClientes` FOREIGN KEY (`cliente`) REFERENCES `clientes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
