DROP DATABASE IF EXISTS taller_vidal;
CREATE DATABASE taller_vidal;
USE taller_vidal;

DROP PROCEDURE IF EXISTS repuestos;
CREATE TABLE repuestos(
  id int AUTO_INCREMENT PRIMARY KEY,
  referencia varchar(25),
  descripcion varchar(300),                             
  proveedor varchar(50),
  marca varchar(25)
);
DROP PROCEDURE IF EXISTS clientes;
CREATE TABLE clientes(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(100),
  apellidos varchar(100),
  rs varchar(100),                                   
  cif_nif varchar(15),
  direccion varchar(150),
  cp int(5),
  localidad varchar(50),
  provincia varchar(50),
  email varchar(50),
  fijo int(9),
  movil int(9)
);
DROP PROCEDURE IF EXISTS vehiculos;
CREATE TABLE vehiculos(
  id int AUTO_INCREMENT PRIMARY KEY,                                
  cliente int(11),
  matricula varchar(10),
  marca varchar(25),
  color varchar(20),
  bastidor varchar(17),
  combustible varchar(25)
);

DROP PROCEDURE IF EXISTS parte1;
CREATE TABLE parte1(
  id int AUTO_INCREMENT PRIMARY KEY,
  vehiculo int(11),
  entrada date,
  salida date,
  nparte int(11),
  nfactura int(11),
  descripcion text,
  estado tinyint,
  kms int(10),
  dto int(2),
  iva int(2)
);

DROP PROCEDURE IF EXISTS parte2;
CREATE TABLE parte2(
  id int AUTO_INCREMENT PRIMARY KEY, 
  id_parte1 int(11),                                     
  codigo varchar(10), 
  descripcion varchar(200),
  cantidad int(3),
  dto int(2),
  importe double(5,2)
);
-- 
-- 
-- 
-- -- FOREIGN KEY clientes con vehiculos
    ALTER TABLE vehiculos
  ADD CONSTRAINT fk_vehiculosClientes FOREIGN KEY (cliente) REFERENCES clientes (id);
-- -- 
-- -- -- FOREIGN KEY vehiculos con parte de cabecera del vehiculo
    ALTER TABLE parte1
  ADD CONSTRAINT fk_vehiculosParte FOREIGN KEY (vehiculo) REFERENCES vehiculos (id);
-- 
-- -- -- FOREIGN KEY parte de cabecera con parte de detalle
    ALTER TABLE parte2
  ADD CONSTRAINT fk_ParteVehiculoParteReparacion FOREIGN KEY (id_parte1) REFERENCES parte1 (id);
-- 
